<?php

namespace backend\views\widgets;

use common\models\Area;
use common\models\DeliveryDoc;
use common\models\Factory;
use common\models\StatusDelivery;
use common\models\TransportCompany;
use yii\base\Widget;

class DeliveryFilterWidget extends Widget
{
    public $title;
    public $showAlerts = false;
    public $filters;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('_delivery-filter', [
            'title' => $this->title,
            'showAlerts' => $this->showAlerts,
            'areas' => Area::find()->orderBy('name')->all(),
            'factories' => Factory::find()->orderBy('name')->all(),
            'transportCompanies' => TransportCompany::find()->orderBy('name')->all(),
            'timeRange' => DeliveryDoc::find()->select('time_range')->distinct()->orderBy('time_range')->all(),
            'statusDeliveries' => StatusDelivery::find()->orderBy('name')->all(),
            'filters' => $this->filters,
        ]);

    }
}