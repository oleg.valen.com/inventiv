<?php

use yii\helpers\Html;

?>

<div class="pink-block">
    <div class="top-nav">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
        <div class="navbar-user">
            <div class="user">
                <div class="user-wrapper show-user-dropdown">
                    <div class="user-name"><?= Yii::$app->user->getIdentity()->surname_name ?></div>
                    <div class="icon-user"></div>
                </div>
                <div class="dropdown-user">
                    <ul>
                        <li>
                            <?= Html::a('Выход', ['site/logout'], [
                                'class' => 'logout',
                                'title' => 'Выход',
                                'data' => ['method' => 'post']]) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header"><?= $title ?></div>
    <form action="#" method="get" data-pjax="1">
        <div class="filter-wrapper">
            <div class="box">
                <h6 class="f-name-height">Территориальный</h6>
                <div class="form-group">
                    <select class="form-control">
                        <option value="0">Все</option>
                        <?php if (false): ?>
                            <?php foreach ($transportCompanies as $item): ?>
                                <option value="<?= $item->id ?>"><?= $item->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <option value="1">Михаил Батусов</option>
                        <option value="2">Виктор Солдатенко</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="box box-date">
                <h6 class="f-name-height">Дата доставки</h6>
                <div class="form-group">
                    <input type="text" class="form-control" value="13-04-2021 - 13-05-2021">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="box">
                <h6 class="f-name-height">Статус доставки</h6>
                <div class="form-group">
                    <select class="form-control">
                        <option value="">Все</option>
                        <option value="1">самовывоз</option>
                        <option value="2">обычная покупка</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="box">
                <h6 class="f-name-height">Магазин</h6>
                <div class="form-group">
                    <select class="form-control">
                        <option value="0">Все</option>
                        <?php if (false): ?>
                            <?php foreach ($timeRange as $key => $item): ?>
                                <option value="<?= $key ?>"><?= $item->time_range ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <option value="1">re:Store Москва Фестиваль</option>
                        <option value="2">re:Store Волгоград Европа</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="box">
                <h6 class="f-name-height">Продавец</h6>
                <div class="form-group">
                    <select class="form-control">
                        <option value="0">Все</option>
                        <?php if (false): ?>
                            <?php foreach ($factories as $item): ?>
                                <option value="<?= $item->id ?>"><?= $item->name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <option value="1">Гаркуша Олег Евгеньевич</option>
                        <option value="2">Дрокова Екатерина Сергеевна</option>
                    </select>
                    <div class="help-block"></div>
                </div>
            </div>
            <input type="hidden" value=""/>
        </div>
    </form>
    <?php if ($showAlerts): ?>
        <div class="alerts-wrap">
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Всего анкет
                        </div>
                        <div class="sum">
                            9997
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-all-anket"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Доля Алертов
                        </div>
                        <div class="sum">
                            7,8 %
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-share-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Просроч. Алерты
                        </div>
                        <div class="sum">
                            73
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-expired-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Новые Алерты
                        </div>
                        <div class="sum">
                            30
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-new-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Алерты в работе
                        </div>
                        <div class="sum">
                            49
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-alert-in-work"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php endif; ?>
</div>