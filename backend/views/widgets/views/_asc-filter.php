<?php

use yii\helpers\Html;

?>

<div class="pink-block">
    <div class="top-nav">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
        <div class="navbar-user">
            <div class="user">
                <div class="user-wrapper show-user-dropdown">
                    <div class="user-name"><?= Yii::$app->user->getIdentity()->surname_name ?></div>
                    <div class="icon-user"></div>
                </div>
                <div class="dropdown-user">
                    <ul>
                        <li>
                            <?= Html::a('Выход', ['site/logout'], [
                                'class' => 'logout',
                                'title' => 'Выход',
                                'data' => ['method' => 'post']]) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header"><?= $title ?></div>
    <form action="#" method="get" data-pjax="1">
        <div class="filter-wrapper">
            <!-- блок выбора расположения -->
            <div class="box">
                <h6 class="f-name-height">Посетитель </h6>
                <div class="form-group">
                    <select class="form-control">
                        <option value="">Все</option>
                        <option value="1">Посетитель 1</option>
                        <option value="2">Посетитель 2</option>
                    </select>

                    <div class="help-block"></div>
                </div>
            </div>
            <!-- блок выбора даты -->
            <div class="box box-date">
                <h6 class="f-name-height">Период</h6>
                <div class="form-group">
                    <input type="text" class="form-control" value="13-04-2021 - 13-05-2021">
                    <div class="help-block"></div>
                </div>
            </div>

            <!-- блок выбора расположения -->
            <div class="box">
                <h6 class="f-name-height">Номер заявки на ремонт </h6>
                <div class="form-group">
                    <select class="form-control">
                        <option value="">Все</option>
                        <option value="1">Заявкa 1</option>
                        <option value="2">Заявкa 2</option>
                    </select>

                    <div class="help-block"></div>
                </div>
            </div>
            <!-- блок выбора расположения -->
            <div class="box">
                <h6 class="f-name-height">Сервисный центр </h6>
                <div class="form-group">
                    <select class="form-control">
                        <option value="">Все</option>
                        <option value="1">Центр 1</option>
                        <option value="2">Центр 2</option>
                    </select>

                    <div class="help-block"></div>
                </div>
            </div>
            <input type="hidden" value="">
        </div>
    </form>
    <?php if ($showAlerts): ?>
        <div class="alerts-wrap">
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Всего анкет
                        </div>
                        <div class="sum">
                            9997
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-all-anket"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Доля Алертов
                        </div>
                        <div class="sum">
                            7,8 %
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-share-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Просроч. Алерты
                        </div>
                        <div class="sum">
                            73
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-expired-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Новые Алерты
                        </div>
                        <div class="sum">
                            30
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-new-alert"></div>
                    </div>
                </a>
            </div>
            <div class="alert-item">
                <a href="#" class="alert-row">
                    <div class="info-col">
                        <div class="name">
                            Алерты в работе
                        </div>
                        <div class="sum">
                            49
                        </div>
                    </div>
                    <div class="icon-col">
                        <div class="icon icon-alert-in-work"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php endif; ?>
</div>