<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="pink-block <?= $deliveryDoc ? ' profile-header' : '' ?>">
    <div class="top-nav">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
        <div class="navbar-user">
            <div class="user">
                <div class="user-wrapper show-user-dropdown">
                    <div class="user-name"><?= Yii::$app->user->getIdentity()->surname_name ?></div>
                    <div class="icon-user"></div>
                </div>
                <div class="dropdown-user">
                    <ul>
                        <li>
                            <?= Html::a('Выход', ['site/logout'], [
                                'class' => 'logout',
                                'title' => 'Выход',
                                'data' => ['method' => 'post']]) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header"><?= $title ?></div>
    <div class="btn-back-wrap">
        <a href="<?= Url::to(['index']) ?>" class="btn-back btn-default">Назад</a>
    </div>
    <?php if ($deliveryDoc): ?>
        <div class="profile-wrapepr">
            <div class="profile-photo-box">
                <span class="photo-title">Профиль клиента</span>
                <div class="photo-container">
                    <?= Html::img('@web/img/user.svg', ['alt' => 'user', 'class' => 'user-img', 'id' => 'profileImage']) ?>
                    <div class="edit">
                        <i class="edit-con"></i>
                    </div>
                    <input id="imageUpload" type="file" name="profile_photo" capture>
                </div>
            </div>
            <div class="profile-info-box">
                <div class="info-row">
                    <div class="info-item">
                        <p class="info-name">Дата доставки</p>
                        <p class="info-number"><?= date('d.m.Y m:H', $deliveryDoc->delivery_date) ?></p>
                    </div>
                    <div class="info-item">
                        <p class="info-name">Статус доставки</p>
                        <p class="info-number">Самовывоз</p>
                    </div>
                    <div class="info-item">
                        <p class="info-name">Временной диапазон доставки</p>
                        <p class="info-number"><?= $deliveryDoc->time_range ?></p>
                    </div>
                </div>
                <div class="info-row">
                    <div class="info-item">
                        <p class="info-name">ТК (транспортная компания)</p>
                        <p class="info-number"><?= $deliveryDoc->transportCompany->name ?></p>
                    </div>
                    <div class="info-item">
                        <p class="info-name">Краткое наименование завода</p>
                        <p class="info-number"><?= $deliveryDoc->factory->name ?></p>
                    </div>
                    <div class="info-item">
                        <p class="info-name">Название почтового региона</p>
                        <p class="info-number"><?= $deliveryDoc->area->name ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>