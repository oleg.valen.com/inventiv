<?php

namespace backend\views\widgets;

use common\models\Area;
use common\models\DeliveryDoc;
use common\models\Factory;
use common\models\TransportCompany;
use yii\base\Widget;

class RetailFilterWidget extends Widget
{
    public $title;
    public $showAlerts = false;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('_retail-filter', [
            'title' => $this->title,
            'showAlerts' => $this->showAlerts,
        ]);

    }
}