<?php

use yii\helpers\Html;
//use yii\bootstrap4\ActiveForm;
use backend\views\widgets\UserFilterWidget;
use yii\helpers\Url;
use common\models\User;
use common\models\DeliverySurvey;
use yii\widgets\ActiveForm;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= UserFilterWidget::widget([
                'deliveryDoc' => $deliveryDoc,
                'title' => 'Опрос b2b / Все анкеты / Профиль клиента',
            ]) ?>
            <div class="client-answer-container">
                <p class="answer-title">Ответы клиента</p>
                <div class="answer-item no-border">
                    <span class="q-text">NPS</span>
                    <div class="answer-box">
                        <div class="point-square">
                            <span class="point">2</span>
                        </div>
                        <div class="line"></div>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Каким образом началось общение с сотрудником магазина?</span>
                    <div class="answer-box">
                        <p class="answer-txt">Сотрудники были свободны, но не проявили активности, я сам(а)
                            инициировал(а) общение</p>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Как проходил выбор товара?</span>
                    <div class="answer-box">
                        <p class="answer-txt">Сотрудник не помогал мне с выбором</p>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Как проходило знакомство с товаром?</span>
                    <div class="answer-box">
                        <p class="answer-txt">Сотрудник только отвечал на мои вопросы</p>
                    </div>
                </div>
                <div class="answer-item">
                            <span class="q-text">Рассказал ли сотрудник магазина о возможностях других товаров и
                                сервисов?</span>
                    <div class="answer-box">
                        <p class="answer-txt">Нет, не рассказывал</p>
                    </div>
                </div>
            </div>
            <div class="btn-wrapper take-to-work">
                <button type="button" class="anket-btn">Взять в работу</button>
            </div>
            <div class="btn-wrapper complete-btn-wrapper">
                <button type="button" class="anket-btn">Завершить выполнение</button>
            </div>
            <div class="comment-container">
                <p>Уважаемая Татьяна!</p>
                <textarea class="comment-field" name="" id=""></textarea>
                <span class="comment-error">Поле обязательно к заполнению</span>
                <div class="send-btn-wrp">
                    <button type="submit" class="send-btn">Отправить</button>
                </div>
            </div>
            <div class="alert-history-container">
                <p class="answer-title">История обработки Алерта</p>
                <div class="history-item">
                    <p class="history-item-name">Время события:</p>
                    <p class="history-item-data">05-10-2021 12:00:32</p>
                </div>
                <div class="history-item">
                    <p class="history-item-name">ФИО:</p>
                    <p class="history-item-data">Синцова Татьяна</p>
                </div>
                <div class="history-item">
                    <p class="history-item-name">Действие:</p>
                    <p class="history-item-data">Взял в работу</p>
                </div>
                <div class="history-item">
                    <p class="history-item-name">Время взятия в работу:</p>
                    <p class="history-item-data">05-10-2021 12:00</p>
                </div>
                <div class="history-item">
                    <p class="history-item-name">Время закрытия Алерта:</p>
                    <p class="history-item-data">06-10-2021 15:30</p>
                </div>
                <div class="history-item">
                    <p class="history-item-name">ФИО:</p>
                    <p class="history-item-data">Синцова Татьяна</p>
                </div>
                <div class="history-item">
                    <p class="history-item-name">Действие:</p>
                    <p class="history-item-data">Закрыл Алерт</p>
                </div>
                <div class="history-item">
                    <p class="history-item-name">Комментарий о выполненой работе:</p>
                    <p class="history-item-data alert-comment">Спасибо, что приняли участие в опросе об уровне
                        нашего сервиса. Мы всегда стремимся к высокому уровню обслуживания клиентов и нам очень
                        жаль, что вы не увидели заинтересованности от наших сотрудников</p>
                </div>
            </div>
        </div>
    </div>
</div>