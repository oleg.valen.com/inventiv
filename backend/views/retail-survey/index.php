<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\views\widgets\RetailFilterWidget;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content bar-charts-content">
            <?= RetailFilterWidget::widget([
                'showAlerts' => true,
                'title' => 'Опрос по рознице / Все анкеты',
            ]) ?>
            <div class="search-row">
                <div class="search-wrap">
                    <form action="#">
                        <input type="text">
                        <button class="search-submit"></button>
                    </form>
                </div>
                <a href="#" class="unload-wrap">
                    <div class="icon">
                    </div>
                    <div class="text">
                        <div class="big-text">
                            Выгрузка
                        </div>
                        <div class="small-text">
                            Скачать файл
                        </div>
                    </div>
                </a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th>
                                Индикатор<br>
                                Алерта
                            </th>
                            <th>
                                Статус<br>
                                Алерта
                            </th>
                            <th>
                                Номер <br>
                                Алерта
                            </th>
                            <th>
                                Дата <br>
                                опроса
                            </th>
                            <th>
                                Оценка<br>
                                NPS
                            </th>
                            <th class="long-col">
                                Магазин
                            </th>
                            <th class="long-col">
                                Териториальный
                            </th>
                            <th class="long-col">
                                Продавец
                            </th>
                            <th>
                                Номер<br>
                                карты
                            </th>
                            <th>
                                Статус<br>
                                доставки
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">
                                    <div class="alert-label">!ALERT</div>
                                </a>
                            </td>
                            <td>
                                Новый
                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                4
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">
                                    <div class="alert-label">!ALERT</div>
                                </a>
                            </td>
                            <td>
                                Новый
                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                3
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">
                                    <div class="alert-label">!ALERT</div>
                                </a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                2
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['retail-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                re:Store Москва
                                Рио Ленинский
                            </td>
                            <td>
                                Михаил Батусов
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                29374690117635
                            </td>
                            <td>
                                Самовывоз
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pagination-wrap">
                <ul class="pagination">
                    <li class="prev disabled"><span>Назад</span></li>
                    <li class="active"><a href="#" data-page="0">1</a></li>
                    <li><a href="#" data-page="1">2</a></li>
                    <li><a href="#" data-page="2">3</a></li>
                    <li><a href="#" data-page="3">4</a></li>
                    <li class="next"><a href="#" data-page="1">Далее</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
