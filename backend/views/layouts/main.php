<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\widgets\Alert;
use backend\assets\AppAsset;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode('reStore') ?></title>
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>

    <?= $content ?>
    <div class="scrollup">
        <div class="arrow-wrap">
            <div class="icon-arrow"></div>
        </div>
    </div>

    <?php if (false): ?>
        <div class="wrapper">
            <div id="header-fix" class="header w-100">
                <nav class="navbar navbar-expand-lg crystal-bg-primary py-0 pl-0 text-white">
                    <a href="<?= Url::home() ?>" class="navbar-brand site-logo crystal-bg-secondry mr-0 pt-4 pl-3"><img
                                src="dist/images/logo-v1.png" alt="" class="img-fluid"/></a>
                    <div class="navbar-header px-3 pt-4 mr-lg-4 crystal-brd-right">
                        <button type="button" id="sidebarCollapse" class="navbar-btn bg-transparent">
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <div class="form-group mb-0 position-relative d-none d-lg-inline-block">
                        <input type="text" class="form-control crystal-rounded-circle-50 border-white"
                               placeholder="Search anything..."/>
                        <div class="btn-search position-absolute">
                            <a href="#" class="crystal-light"><i class="fa fa-search"></i></a>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav notification ml-auto">
                            <li class="nav-item dropdown align-self-center">
                                <a class="nav-link position-relative px-3 py-4" data-toggle="dropdown"
                                   aria-expanded="false"><span class="ti-email"></span>
                                    <span class="ring-point rounded-circle">
                                        <span class="ring"></span>
                                    </span>
                                </a>
                                <ul class="dropdown-menu border mailbox crystal-border-light border-top-0 rounded-0 py-0">
                                    <li class="px-3 py-2">
                                        <a href="#"><h6 class="mb-0">You Have 3 New Message</h6></a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">
                                            <div class="media">
                                                <img src="dist/images/author5.jpg" alt=""
                                                     class="d-flex mr-3 img-fluid crystal-rounded-circle-50 align-self-center"
                                                     width="45"/>
                                                <div class="media-body">
                                                    <b class="crystal-dark">John Smith</b>
                                                    <small class="crystal-light d-block"> Lorem ipsum dolor</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">
                                            <div class="media">
                                                <img src="dist/images/author6.jpg" alt=""
                                                     class="d-flex mr-3 img-fluid crystal-rounded-circle-50 align-self-center"
                                                     width="45"/>
                                                <div class="media-body">
                                                    <b class="crystal-dark"> Lisa Wong </b>
                                                    <small class="crystal-light d-block"> Lorem ipsum dolor</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">
                                            <div class="media">
                                                <img src="dist/images/author7.jpg" alt=""
                                                     class="d-flex mr-3 img-fluid crystal-rounded-circle-50 align-self-center"
                                                     width="45"/>
                                                <div class="media-body">
                                                    <b class="crystal-dark">Caleb Richards </b>
                                                    <small class="crystal-light d-block"> Lorem ipsum dolor</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li><a class="dropdown-item text-center" href="#"> Read All Message <i
                                                    class="fa fa-angle-right pl-2"></i></a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown align-self-center">
                                <a class="nav-link position-relative px-3 py-4" data-toggle="dropdown"
                                   aria-expanded="false"><span class="ti-bell"></span>
                                    <span class="ring-point rounded-circle">
                                        <span class="ring"></span>
                                    </span>
                                </a>
                                <ul class="dropdown-menu border mailbox crystal-border-light border-top-0 rounded-0 py-0">
                                    <li class="px-3 py-2">
                                        <a href="#"><h6 class="mb-0">Notification <span
                                                        class="badge badge-primary float-right mt-1">New 3</span></h6>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">
                                            <div class="media">
                                                <div class="d-flex mr-2 rounded-circle p-2 crystal-bg-primary text-white align-self-center">
                                                    <i class="fa fa-book"></i></div>
                                                <div class="media-body crystal-line-height-1_8">
                                                    <small class="crystal-dark font-weight-bold">A new order has been
                                                        placed</small>
                                                    <small class="d-block">2 hours ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">
                                            <div class="media">
                                                <div class="d-flex mr-2 rounded-circle p-2 crystal-bg-primary text-white align-self-center">
                                                    <i class="fa fa-user"></i></div>
                                                <div class="media-body crystal-line-height-1_8">
                                                    <small class="crystal-dark font-weight-bold">Complete The
                                                        Task</small>
                                                    <small class="d-block">2 days ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">
                                            <div class="media">
                                                <div class="d-flex mr-2 rounded-circle p-2 crystal-bg-primary text-white align-self-center">
                                                    <i class="fa fa-cog"></i></div>
                                                <div class="media-body crystal-line-height-1_8">
                                                    <small class="crystal-dark font-weight-bold">Setting Updated</small>
                                                    <small class="d-block">3 days ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#">
                                            <div class="media">
                                                <div class="d-flex mr-2 rounded-circle p-2 crystal-bg-primary text-white align-self-center">
                                                    <i class="fa fa-calendar"></i></div>
                                                <div class="media-body crystal-line-height-1_8">
                                                    <small class="crystal-dark font-weight-bold">Event Started</small>
                                                    <small class="d-block">8 days ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li><a class="dropdown-item text-center" href="#"> All Notification</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown align-self-center mr-3">
                                <a class="nav-link position-relative p-3" data-toggle="dropdown" aria-expanded="false">
                                    <div class="media">
                                        <img src="dist/images/author1.jpg" alt=""
                                             class="img-fluid d-flex mr-3 rounded-circle" width="44">
                                        <div class="media-body crystal-line-height-1_5 align-self-center text-white">
                                            <small class="text-uppercase">John Deo</small>
                                            <small class="d-block">Devloper</small>
                                        </div>
                                    </div>
                                </a>
                                <ul class="dropdown-menu border crystal-border-light border-top-0 rounded-0 py-0 font-weight-bold">
                                    <li>
                                        <a class="dropdown-item" href="#"><span class="ti-user pr-2"></span> My Profile</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#"><span class="ti-email pr-2"></span> Inbox</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#"><span class="ti-settings pr-2"></span> Account
                                            Setting</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="#"><span class="ti-power-off pr-2"></span> Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <nav id="sidebar">
                <div class="sidebar-nav">
                    <ul class="metismenu list-unstyled mb-0" id="menu">
                        <li><a href="<?= Url::home() ?>"
                               class="navbar-brand site-logo crystal-bg-secondry mr-0 pt-4 pl-3"><img
                                        src="dist/images/logo-v1.png" alt="" class="img-fluid"/></a></li>


                        <li class="my-4 nav-title position-relative"><span
                                    class="text-white mb-0">Main Menu Links</span>
                        </li>
                        <li class="active"><a class="active" href="<?= Url::home() ?>"><i class="ti-home pr-1"></i>
                                Dashboard</a></li>
                        <li>
                            <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i
                                        class="ti-file pr-1"></i> Pages</a>
                            <ul class="collapse list-unstyled">
                                <li><a href="<?= Url::to(['/lockscreen']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Lockscreen</a>
                                </li>
                                <li><a href="<?= Url::to(['/login']) ?>"><i class="ti-angle-right pr-1"></i> login</a>
                                </li>
                                <li><a href="<?= Url::to(['/register']) ?>"><i class="ti-angle-right pr-1"></i>Register</a>
                                </li>
                                <li><a href="<?= Url::to(['/404']) ?>"><i class="ti-angle-right pr-1"></i> 404 Page</a>
                                </li>
                                <li><a href="<?= Url::to(['/blank']) ?>"><i class="ti-angle-right pr-1"></i> Blank Page</a>
                                </li>
                                <li><a href="<?= Url::to(['/faq']) ?>"><i class="ti-angle-right pr-1"></i> Faq</a></li>
                                <li><a href="<?= Url::to(['/user-list']) ?>"><i class="ti-angle-right pr-1"></i> User
                                        List</a></li>
                                <li><a href="<?= Url::to(['/user-profile']) ?>"><i class="ti-angle-right pr-1"></i> User
                                        Profile</a></li>
                                <li><a href="<?= Url::to(['/tabs']) ?>"><i class="ti-angle-right pr-1"></i> Tabs</a>
                                </li>
                                <li><a href="<?= Url::to(['/grid-options']) ?>"><i class="ti-angle-right pr-1"></i> Grid
                                        Options</a></li>
                                <li><a href="<?= Url::to(['/timeline']) ?>"><i class="ti-angle-right pr-1"></i> Timeline</a>
                                </li>
                                <li><a href="<?= Url::to(['/modals']) ?>"><i class="ti-angle-right pr-1"></i> Modals</a>
                                </li>
                                <li><a href="<?= Url::to(['/video']) ?>"><i class="ti-angle-right pr-1"></i> Video</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i
                                        class="ti-email pr-1"></i> Email</a>
                            <ul class="collapse list-unstyled">
                                <li><a href="<?= Url::to(['/email-inbox']) ?>"><i class="ti-angle-right pr-1"></i> Inbox</a>
                                </li>
                                <li><a href="<?= Url::to(['/email-composs']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Composs
                                        Mail </a></li>
                                <li><a href="<?= Url::to(['/email-details']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Email
                                        Details</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i
                                        class="ti-bag pr-1"></i> UI Kit </a>
                            <ul class="collapse list-unstyled">
                                <li><a href="<?= Url::to(['/alerts']) ?>"><i class="ti-angle-right pr-1"></i> Alerts</a>
                                </li>
                                <li><a href="<?= Url::to(['/buttons']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Buttons</a>
                                </li>
                                <li><a href="<?= Url::to(['/cards']) ?>"><i class="ti-angle-right pr-1"></i> Cards</a>
                                </li>
                                <li><a href="<?= Url::to(['/icons']) ?>"><i class="ti-angle-right pr-1"></i> Icons</a>
                                </li>
                                <li><a href="<?= Url::to(['/typography']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Typography</a>
                                </li>
                                <li><a href="<?= Url::to(['/chat']) ?>"><i class="ti-angle-right pr-1"></i> Chat</a>
                                </li>
                                <li><a href="<?= Url::to(['/steps']) ?>"><i class="ti-angle-right pr-1"></i> Steps</a>
                                </li>
                                <li><a href="<?= Url::to(['/pricing']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Pricing</a>
                                </li>
                                <li><a href="<?= Url::to(['/sweet-alert']) ?>"><i class="ti-angle-right pr-1"></i> Sweet
                                        Alert</a></li>
                                <li><a href="<?= Url::to(['/toastr-alert']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Toastr
                                        Alert</a></li>
                                <li><a href="<?= Url::to(['/progress-bar']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Progress
                                        Bar</a></li>
                                <li><a href="<?= Url::to(['/contact-us']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Contact Us</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i
                                        class="ti-pencil-alt pr-1"></i> Form</a>
                            <ul class="collapse list-unstyled">
                                <li><a href="<?= Url::to(['/form-elements']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Form
                                        Elements</a></li>
                                <li><a href="<?= Url::to(['/form-layout']) ?>"><i class="ti-angle-right pr-1"></i> Form
                                        Layout</a></li>
                                <li><a href="<?= Url::to(['/form-upload']) ?>"><i class="ti-angle-right pr-1"></i> Form
                                        Upload</a></li>
                                <li><a href="<?= Url::to(['/form-text-editor']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Form
                                        Text Editor</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i
                                        class="ti-bar-chart pr-1"></i> Chart</a>
                            <ul class="collapse list-unstyled">
                                <li><a href="<?= Url::to(['/chart-chartist']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Chartist Chart</a></li>
                                <li><a href="<?= Url::to(['/chart-chartjs']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Chartjs
                                        Chart</a></li>
                                <li><a href="<?= Url::to(['/chart-knob']) ?>"><i class="ti-angle-right pr-1"></i> Knob
                                        Chart</a>
                                </li>
                                <li><a href="<?= Url::to(['/chart-morris']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Morris
                                        Chart</a></li>
                                <li><a href="<?= Url::to(['/chart-sparkline']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Sparkline Chart</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i
                                        class="ti-layout-grid4 pr-1"></i> Tables</a>
                            <ul class="collapse list-unstyled">
                                <li><a href="<?= Url::to(['/table-basic']) ?>"><i class="ti-angle-right pr-1"></i> Basic
                                        Table</a></li>
                                <li><a href="<?= Url::to(['/table-responsive']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Responsive Table</a></li>
                                <li><a href="<?= Url::to(['/table-bootstrap']) ?>"><i class="ti-angle-right pr-1"></i>
                                        Bootstrap Table</a></li>
                                <li><a href="<?= Url::to(['/table-data']) ?>"><i class="ti-angle-right pr-1"></i> Data
                                        Table</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?= Url::to(['/calendar']) ?>"><i class="ti-calendar pr-1"></i> Calendar</a></li>
                        <li>
                            <a class="has-arrow" href="#" data-toggle="collapse" aria-expanded="false"><i
                                        class="ti-map pr-1"></i> Map</a>
                            <ul class="collapse list-unstyled">
                                <li><a href="<?= Url::to(['/map-google']) ?>"><i class="ti-angle-right pr-1"></i> Google
                                        Map</a>
                                </li>
                                <li><a href="<?= Url::to(['/map-vector']) ?>"><i class="ti-angle-right pr-1"></i> Vector
                                        Map</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                </div>
            </nav>
            <?= $content ?>

            <!-- Top To Bottom-->
            <div class="scrollup text-center crystal-bg-primary">
                <a href="#" class="text-white"><i class="fa fa-chevron-up"></i></a>
            </div>
            <!-- End Top To Bottom-->

        </div>
    <?php endif; ?>

    <footer>
    </footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage();
