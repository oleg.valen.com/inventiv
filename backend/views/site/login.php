<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-page">
    <div class="login-box">
        <div class="login-box-body">
            <p class="login-box-header">Авторизация</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Логин" aria-required="true" autocomplete
                       name="LoginForm[username]">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Пароль" aria-required="true" autocomplete
                       name="LoginForm[password]">
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label for="loginform-rememberme">
                        <input type="checkbox" id="loginform-rememberme" name="LoginForm[rememberMe]">
                        Запомнить меня
                    </label>
                </div>
            </div>
            <div class="btn-wrap">
                <button type="submit" class="btn-submit" name="login-button">Войти</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>