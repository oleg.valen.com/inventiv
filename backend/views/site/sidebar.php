<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;

?>

<div class="sidebar">
    <div class="menu-sidebar">
        <div class="logo">
            <a href="<?= Url::home() ?>">
                <?= Html::img('@web/img/logo.jpg', ['alt' => 'reStore']) ?>
            </a>
        </div>
        <div class="menu">
            <nav>
                <ul class="sidebar-nav">
                    <?php if (Yii::$app->user->can('viewRetail')): ?>
                        <li class="treeview <?= (in_array($id, ['site', 'retail-survey', 'retail-table']) ? 'active' : '') ?>">
                            <a href="<?= Url::home() ?>">
                                <div class="icon icon-chart"></div>
                                Опрос по рознице
                            </a>
                            <div class="hidden-menu">
                                <ul>
                                    <li class="<?= $id == 'site' ? 'active' : '' ?>">
                                        <a href="<?= Url::home() ?>">Ключевые показатели</a>
                                    </li>
                                    <li class="<?= $id == 'retail-survey' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/retail-survey']) ?>">Все анкеты</a>
                                    </li>
                                    <li class="<?= $id == 'retail-table' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/retail-table']) ?>">Сводная информация</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('viewDelivery')): ?>
                        <li class="treeview <?= (in_array($id, ['delivery-indicator', 'delivery-survey', 'delivery-table']) ? 'active' : '') ?>">
                            <a href="#">
                                <div class="icon icon-rocket"></div>
                                Опрос по доставке
                            </a>
                            <div class="hidden-menu">
                                <ul>
                                    <li class="<?= $id == 'delivery-indicator' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/delivery-indicator']) ?>">Ключевые показатели</a>
                                    </li>
                                    <li class="<?= $id == 'delivery-survey' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/delivery-survey']) ?>">Все анкеты</a>
                                    </li>
                                    <li class="<?= $id == 'delivery-table' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/delivery-table']) ?>">Сводная информация</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('viewB2b')): ?>
                        <li class="treeview <?= (in_array($id, ['b2b-indicator', 'b2b-survey', 'b2b-table']) ? 'active' : '') ?>">
                            <a href="#">
                                <div class="icon icon-file"></div>
                                Опрос b2b
                            </a>
                            <div class="hidden-menu">
                                <ul>
                                    <li class="<?= $id == 'b2b-indicator' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/b2b-indicator']) ?>">Ключевые показатели</a>
                                    </li>
                                    <li class="<?= $id == 'b2b-survey' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/b2b-survey']) ?>">Все анкеты</a>
                                    </li>
                                    <li class="<?= $id == 'b2b-table' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/b2b-table']) ?>">Сводная информация</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('viewAsc')): ?>
                        <li class="treeview <?= (in_array($id, ['asc-indicator', 'asc-survey', 'asc-table']) ? 'active' : '') ?>">
                            <a href="#">
                                <div class="icon icon-card"></div>
                                Опрос АСЦ
                            </a>
                            <div class="hidden-menu">
                                <ul>
                                    <li class="<?= $id == 'asc-indicator' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/asc-indicator']) ?>">Ключевые показатели</a>
                                    </li>
                                    <li class="<?= $id == 'asc-survey' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/asc-survey']) ?>">Все анкеты</a>
                                    </li>
                                    <li class="<?= $id == 'asc-table' ? 'active' : '' ?>">
                                        <a href="<?= Url::to(['/asc-table']) ?>">Сводная информация</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="<?= Url::to(['/user']) ?>">
                            <div class="icon icon-users"></div>
                            Пользователи
                        </a>
                    </li>
                    <li>
                        <a href="/admin/mystery-shopping/">
                            <div class="icon icon-shopping"></div>
                            Mystery shopping
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
