<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar">
        <div class="menu-sidebar">
            <div class="logo">
                <a href="<?= Url::home() ?>">
                    <?= Html::img('@web/img/logo.png', ['alt' => 'reStore']) ?>
                </a>
            </div>
            <div class="menu">
                <nav>
                    <ul class="sidebar-nav">
                        <li class="treeview active">
                            <a href="<?= Url::home() ?>">
                                <div class="icon icon-chart"></div>
                                Опрос по рознице
                            </a>
                            <div class="hidden-menu">
                                <ul>
                                    <li>
                                        <a href="<?= Url::home() ?>">Ключевые показатели</a>
                                    </li>
                                    <li>
                                        <a href="<?= Url::to(['/all-ankets']) ?>">Все анкеты</a>
                                    </li>
                                    <li class="active">
                                        <a href="<?= Url::to(['/table']) ?>">Сводная информация</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <div class="icon icon-rocket"></div>
                                Опрос по доставке
                            </a>
                            <div class="hidden-menu">
                                <ul>
                                    <li>
                                        <a href="#">Ключевые показатели</a>
                                    </li>
                                    <li>
                                        <a href="#">Все анкеты</a>
                                    </li>
                                    <li>
                                        <a href="#">Сводная информация</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon icon-file"></div>
                                Опрос b2b
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon icon-card"></div>
                                Опрос АСЦ
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="content" id="content">
        <div class="page-content">
            <div class="pink-block">
                <div class="page-header">
                    Опрос по рознице / Сводная информация
                </div>
                <form action="#" method="get">
                    <div class="filter-wrapper">
                        <!-- блок выбора даты -->
                        <div class="box box-date">
                            <h6>Период</h6>
                            <div class="form-group">
                                <input type="text" class="form-control" value="13-04-2021 - 13-05-2021">
                                <div class="help-block"></div>
                            </div>
                        </div>

                        <!-- блок выбора расположения -->
                        <div class="box">
                            <h6>Статус доставки </h6>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Все</option>
                                    <option value="1">Самовывоз</option>
                                    <option value="2">Самовывоз</option>
                                </select>

                                <div class="help-block"></div>
                            </div>
                        </div>
                        <!-- блок выбора расположения -->
                        <div class="box">
                            <h6>Статус доставки </h6>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Все</option>
                                    <option value="1">Самовывоз</option>
                                    <option value="2">Самовывоз</option>
                                </select>

                                <div class="help-block"></div>
                            </div>
                        </div>
                        <!-- блок выбора расположения -->
                        <div class="box">
                            <h6>Статус доставки </h6>
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">Все</option>
                                    <option value="1">Самовывоз</option>
                                    <option value="2">Самовывоз</option>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="search-row">
                <a href="#" class="unload-wrap">
                    <div class="icon">
                    </div>
                    <div class="text">
                        <div class="big-text">
                            Выгрузка
                        </div>
                        <div class="small-text">
                            Скачать файл
                        </div>
                    </div>
                </a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th rowspan="2" class="long-col">
                                Териториальный
                            </th>
                            <th rowspan="2" class="long-col">
                                Магазин
                            </th>
                            <th rowspan="2" class="long-col">
                                Продавец
                            </th>
                            <th rowspan="2">
                                Отправлено<br>
                                приглашений
                            </th>
                            <th rowspan="2">
                                Количество <br>
                                заполненых<br>
                                анкет
                            </th>
                            <th rowspan="2">
                                Оценка<br>
                                NPS
                            </th>
                            <th rowspan="2" class="long-col">
                                Общение с <br>
                                сотрудником
                            </th>
                            <th rowspan="2" class="long-col">
                                Выбор товара
                            </th>
                            <th rowspan="2" class="long-col">
                                Знакомство <br>
                                с товаром
                            </th>
                            <th rowspan="2" class="long-col">
                                Консультация <br>
                                сотрудника
                            </th>
                            <th rowspan="2" class="long-col">
                                Завершение <br>
                                взаимодействия <br>
                                с сотрудником
                            </th>
                            <th rowspan="2">
                                Доля <br>
                                Алертов (%)
                            </th>
                            <th colspan="5" class="th-alert">
                                <div>
                                    Алерты
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th>Всего</th>
                            <th>
                                Не взят в <br>
                                работу (шт/%)
                            </th>
                            <th>
                                Не обработан <br>
                                в срок (шт/%)
                            </th>
                            <th>
                                В работе <br>
                                (шт/%)
                            </th>
                            <th>
                                Закрыто <br>
                                (шт/%)
                            </th>
                        </tr>
                        </thead>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Батусов
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Батусов
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Батусов
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Батусов
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div class="open-next-row-2">
                                    re:Store Москва
                                    Рио Ленинский
                                </div>
                            </td>
                            <td></td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-2">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Морозов Александр
                                Юрьевич
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Иванов Иван Иванович
                            </td>
                            <td>
                                981
                            </td>
                            <td>
                                76
                            </td>
                            <td>
                                8,9
                            </td>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>8,9</td>
                            <td>5,5</td>
                            <td>7,4</td>
                            <td>2,1</td>
                            <td>3,7</td>
                            <td>4,1</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pagination-wrap">
                <ul class="pagination">
                    <li class="prev disabled"><span>Назад</span></li>
                    <li class="active"><a href="#" data-page="0">1</a></li>
                    <li><a href="#" data-page="1">2</a></li>
                    <li><a href="#" data-page="2">3</a></li>
                    <li><a href="#" data-page="3">4</a></li>
                    <li class="next"><a href="#" data-page="1">Далее</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>