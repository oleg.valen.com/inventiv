<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;
use backend\views\widgets\RetailFilterWidget;

?>

    <div class="container">
        <div class="header">
            <div class="navbar-aside">
                <div class="mobile-menu btn-aside-menu toggle-sidebar">
                    <div class="b-menu">
                        <div class="b-bun b-bun--top"></div>
                        <div class="b-bun b-bun--mid"></div>
                        <div class="b-bun b-bun--bottom"></div>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('/site/sidebar', [
            'id' => $id,
        ]) ?>
        <div class="content" id="content">
            <div class="page-content">
                <?= RetailFilterWidget::widget([
                    'title' => 'Опрос по рознице / Ключевые показатели',
                ]) ?>
                <div class="box-wrapper nps-row">
                    <h3>
                        NPS
                    </h3>
                    <div class="main-nps">
                        <div>
                            <table class="block-table table-striped" id="faceectable">
                                <tbody>
                                <tr>
                                    <td class="cntr_num" colspan="7">
                                        DETRACTORS
                                    </td>
                                    <td class="cntr_num" colspan="2">
                                        PASSIVES
                                    </td>
                                    <td class="cntr_num" colspan="2">
                                        PROMOTERS
                                    </td>
                                </tr>
                                <tr>
                                    <td class="cntr_num" colspan="7">
                                        <div class="planka-long"></div>
                                    </td>
                                    <td class="cntr_num" colspan="2">
                                        <div class="planka-long"></div>
                                    </td>
                                    <td class="cntr_num" colspan="2">
                                        <div class="planka-long"></div>
                                    </td>
                                </tr>
                                <tr class="rate-th">
                                    <td>
                                        <div class="rate-item">0</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">1</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">2</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">3</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">4</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">5</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">6</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">7</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">8</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">9</div>
                                    </td>
                                    <td>
                                        <div class="rate-item">10</div>
                                    </td>
                                </tr>
                                <tr class="dig-td">
                                    <td>
                                        <p class="sum-bot">24</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">43</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">1</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">32</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">52</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">23</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">43</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">22</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">23</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">331</p>
                                    </td>
                                    <td>
                                        <p class="sum-bot">439</p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="left-block">
                            <div class="promot-wrap">
                                <div class="label-promot">
                                    <div class="table-txt11-sp">35</div>
                                    <div class="table-txt12-sp">Promoters</div>
                                    <div class="table-txt13-sp">60.5%</div>
                                </div>
                                <div class="label-promot">
                                    <div class="table-txt11-sp">77</div>
                                    <div class="table-txt12-sp">Passives</div>
                                    <div class="table-txt13-sp">18.5%</div>
                                </div>
                                <div class="label-promot">
                                    <div class="table-txt11-sp">13</div>
                                    <div class="table-txt12-sp">Detractors</div>
                                    <div class="table-txt13-sp">21.0%</div>
                                </div>
                            </div>
                            <div class="block-circle">
                                <div class="circle">
                                    <span>Total</span>
                                    <span>53.4%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box-wrapper box-chart">
                        <h3>
                            Каким образом началось Ваше общение с сотрудниками магазина?
                        </h3>
                        <div class="chart-content">
                            <div class="lineDot">
                                <div class="chart">
                                    <div class="chart-1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-wrapper box-chart">
                        <h3>
                            Как проходил выбор товара?
                        </h3>
                        <div class="chart-content">
                            <div class="lineDot">
                                <div class="chart">
                                    <div class="chart-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box-wrapper box-chart">
                        <h3>
                            Как происходило знакомство с устройством?
                        </h3>
                        <div class="chart-content">
                            <div class="lineDot">
                                <div class="chart">
                                    <div class="chart-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-wrapper box-chart">
                        <h3>
                            Рассказал ли сотрудник о возможностях других товаров и сервисов?
                        </h3>
                        <div class="chart-content">
                            <div class="lineDot">
                                <div class="chart">
                                    <div class="chart-4">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-row">
                    <div class="box-wrapper box-chart">
                        <h3>
                            Как завершилось взаимодействие с сотрудником?
                        </h3>
                        <div class="chart-content">
                            <div class="lineDot">
                                <div class="chart">
                                    <div class="chart-5">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        //data for shart
        let chartData = [
            {
                element: '.chart-1', // class for element
                type: 'lineDot',               //type of chart
                dotColor: '#FFD1E2',
                data: [                          //data
                    {
                        backgroundStart: "#FD2679",
                        backgroundEnd: "#FFD1E2",
                        progress: "45",
                        labelText: "Сотрудник магазина сам инициировал общение и был активен"
                    },
                    {
                        backgroundStart: "#FD2679",
                        backgroundEnd: "#FFD1E2",
                        progress: "32",
                        labelText: "Я сам обратился к сотруднику магазина как только зашел в магазин"
                    },
                    {
                        backgroundStart: "#FD2679",
                        backgroundEnd: "#FFD1E2",
                        progress: "63",
                        labelText: "Сотрудники были свободны, но не проявляли активности, я сам(а) инициировал(а) общение"
                    },
                    {
                        backgroundStart: "#FD2679",
                        backgroundEnd: "#FFD1E2",
                        progress: "40",
                        labelText: "Другое"
                    }
                ],
            },
            {
                element: '.chart-2', // class for element
                type: 'lineDot',               //type of chart
                dotColor: '#F3A6FF',
                data: [                          //data
                    {
                        backgroundStart: "#CC6ADC",
                        backgroundEnd: "#E9C5EF ",
                        progress: "30",
                        labelText: "Я пришел(ла) за определенным товаром и сразу сообщил(а) об этом сотруднику магазина"
                    },
                    {
                        backgroundStart: "#BA7BC4",
                        backgroundEnd: "#CFBED2",
                        progress: "64",
                        labelText: "Сотрудник задавал различные вопросы, уточнял что для меня важно и необходимо"
                    },
                    {
                        backgroundStart: "#DF9EE9",
                        backgroundEnd: "#E8DCE1",
                        progress: "39",
                        labelText: "Сотрудник не помогал мне с выбором"
                    },
                    {
                        backgroundStart: "#E7C2EB",
                        backgroundEnd: "#FCEAFF",
                        progress: "49",
                        labelText: "Другое"
                    }
                ],
            },
            {
                element: '.chart-3', // class for element
                type: 'lineDot',               //type of chart
                dotColor: '#A8A6F2',
                data: [                          //data
                    {
                        backgroundStart: "#413DFF",
                        backgroundEnd: "#ADACFF ",
                        progress: "57",
                        labelText: "Сотрудник показывал возможности устройства и дал мне попробовать их в действии"
                    },
                    {
                        backgroundStart: "#5450F1",
                        backgroundEnd: "#918FED",
                        progress: "40",
                        labelText: "Сотрудник только рассказывал об устройстве"
                    },
                    {
                        backgroundStart: "#6E6BEF",
                        backgroundEnd: "#A9A7F2",
                        progress: "35",
                        labelText: "Сотрудник только отвечал на мои вопросы"
                    },
                    {
                        backgroundStart: "#8F8DF3",
                        backgroundEnd: "#BDBBFF",
                        progress: "54",
                        labelText: "Ничего из выше перечисленного"
                    },
                    {
                        backgroundStart: "#A6A3FD",
                        backgroundEnd: "#E6E5FF",
                        progress: "28",
                        labelText: "Другое"
                    }
                ],
            },
            {
                element: '.chart-4', // class for element
                type: 'lineDot',               //type of chart
                dotColor: '#78D14E',
                data: [                          //data
                    {
                        backgroundStart: "#7DD353",
                        backgroundEnd: "#D2FFBF ",
                        progress: "28",
                        labelText: "Да, информация была полезной, появилось желание купить"
                    },
                    {
                        backgroundStart: "#90D96B",
                        backgroundEnd: "#D5FFC0",
                        progress: "70",
                        labelText: "Да, но предложение было неуместным"
                    },
                    {
                        backgroundStart: "#A3DF82",
                        backgroundEnd: "#E1FED1",
                        progress: "42",
                        labelText: "Нет, не рассказывал"
                    },
                    {
                        backgroundStart: "#A0EEB2",
                        backgroundEnd: "#D6FFDF",
                        progress: "49",
                        labelText: "Другое"
                    }
                ],
            },
            {
                element: '.chart-5', // class for element
                type: 'lineDot',               //type of chart
                dotColor: '#F3A6FF',
                data: [                          //data
                    {
                        backgroundStart: "#BC57B7",
                        backgroundEnd: "#FFDEFD ",
                        progress: "28",
                        labelText: "Сотрудник поздравил с покупкой. Остались приятные впечатления"
                    },
                    {
                        backgroundStart: "#EB72E6",
                        backgroundEnd: "#FED9FC",
                        progress: "67",
                        labelText: "Сотрудник равнодушно попрощался"
                    },
                    {
                        backgroundStart: "#DB84D1",
                        backgroundEnd: "#FEDEFA",
                        progress: "54",
                        labelText: "Другое"
                    }
                ],
            },
        ];
    </script>


<?php if (false): ?>
    <div class="site-index">

        <div class="jumbotron text-center bg-transparent">
            <h1 class="display-4">Congratulations!</h1>

            <p class="lead">You have successfully created your Yii-powered application.</p>

            <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
        </div>

        <div class="body-content">

            <div class="row">
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                        aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/doc/">Yii Documentation
                            &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                        aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/forum/">Yii Forum
                            &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                        aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu
                        fugiat nulla pariatur.</p>

                    <p><a class="btn btn-outline-secondary" href="http://www.yiiframework.com/extensions/">Yii
                            Extensions &raquo;</a></p>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>