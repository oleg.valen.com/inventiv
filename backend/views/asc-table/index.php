<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\views\widgets\AscFilterWidget;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= AscFilterWidget::widget([
                'title' => 'Опрос АСЦ / Сводная информация',
            ]) ?>
            <div class="search-row">
                <a href="#" class="unload-wrap">
                    <div class="icon">
                    </div>
                    <div class="text">
                        <div class="big-text">
                            Выгрузка
                        </div>
                        <div class="small-text">
                            Скачать файл
                        </div>
                    </div>
                </a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th rowspan="2" class="long-col">
                                Сервисный центр
                            </th>
                            <th rowspan="2">
                                Отправлено<br>
                                приглашений
                            </th>
                            <th rowspan="2">
                                Количество <br>
                                заполненых<br>
                                анкет
                            </th>
                            <th rowspan="2">
                                Оценка<br>
                                NPS
                            </th>
                            <th rowspan="2" class="long-col">
                                Как Вы оцениваете <br>
                                качество проведенных<br>
                                работ
                            </th>
                            <th rowspan="2" class="long-col">
                                Насколько время <br>
                                ремонта/обслуживания <br>
                                соответствует ожиданиям?
                            </th>
                            <th rowspan="2" class="long-col">
                                Насколько скорость <br>
                                ремонта/обслуживания <br>
                                соответствует ожиданиям?
                            </th>
                            <th rowspan="2" class="long-col">
                                Насколько были <br>
                                удовлетворены посещением <br>
                                сервисного центра в целом?
                            </th>
                            <th rowspan="2">
                                Доля <br>
                                Алертов (%)
                            </th>
                            <th colspan="5" class="th-alert">
                                <div>
                                    Алерты
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th>Всего</th>
                            <th>
                                Не взят в <br>
                                работу (шт/%)
                            </th>
                            <th>
                                Не обработан <br>
                                в срок (шт/%)
                            </th>
                            <th>
                                В работе <br>
                                (шт/%)
                            </th>
                            <th>
                                Закрыто <br>
                                (шт/%)
                            </th>
                        </tr>
                        </thead>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                Сервисный центр 1
                            </td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                Сервисный центр 2
                            </td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                Сервисный центр 3
                            </td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                Сервисный центр 4
                            </td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
