<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\views\widgets\B2bFilterWidget;
use yii\helpers\Url;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= B2bFilterWidget::widget([
                'showAlerts' => true,
                'title' => 'Опрос b2b / Все анкеты',
            ]) ?>
            <div class="search-row">
                <div class="search-wrap">
                    <form action="#">
                        <input type="text">
                        <button class="search-submit"></button>
                    </form>
                </div>
                <a href="#" class="unload-wrap">
                    <div class="icon">
                    </div>
                    <div class="text">
                        <div class="big-text">
                            Выгрузка
                        </div>
                        <div class="small-text">
                            Скачать файл
                        </div>
                    </div>
                </a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th>
                                Индикатор<br>
                                Алерта
                            </th>
                            <th>
                                Статус<br>
                                Алерта
                            </th>
                            <th>
                                Номер <br>
                                Алерта
                            </th>
                            <th>
                                Дата <br>
                                опроса
                            </th>
                            <th>
                                Оценка<br>
                                NPS
                            </th>
                            <th>
                                ID
                            </th>
                            <th>
                                Дата
                            </th>
                            <th class="long-col">
                                Укажите наименование компании, <br>
                                на которую была совершена покупка
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                523535235
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 1
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">
                                    <div class="alert-label">!ALERT</div>
                                </a>
                            </td>
                            <td>
                                Новый
                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                4
                            </td>
                            <td>
                                786545
                            </td>
                            <td>
                                12.03.2021 15:30
                            </td>
                            <td>
                                Компания 1
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>


                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                54635675
                            </td>
                            <td>
                                06.05.2021 12:44
                            </td>
                            <td>
                                Компания 2
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                7678546
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 2
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                67985746
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 4
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">
                                    <div class="alert-label">!ALERT</div>
                                </a>
                            </td>
                            <td>
                                Новый
                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                5
                            </td>
                            <td>
                                079685
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 2
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                677685435
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 4
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                7657465
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 3
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                76543
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 2
                            </td>
                        </tr>
                        <tr class="alert-row">
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">
                                    <div class="alert-label">!ALERT</div>
                                </a>
                            </td>
                            <td>
                                Новый
                            </td>
                            <td>
                                125363
                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                3
                            </td>
                            <td>
                                768545
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 3
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="<?= Url::to(['b2b-survey/update', 'survey_id' => 17]) ?>">-</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                12.11.2021 12:43
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                6857465
                            </td>
                            <td>
                                10.08.2021 12:44
                            </td>
                            <td>
                                Компания 1
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pagination-wrap">
                <ul class="pagination">
                    <li class="prev disabled"><span>Назад</span></li>
                    <li class="active"><a href="#" data-page="0">1</a></li>
                    <li><a href="#" data-page="1">2</a></li>
                    <li><a href="#" data-page="2">3</a></li>
                    <li><a href="#" data-page="3">4</a></li>
                    <li class="next"><a href="#" data-page="1">Далее</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>