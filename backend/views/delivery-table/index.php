<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\views\widgets\DeliveryFilterWidget;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= DeliveryFilterWidget::widget([
                'title' => 'Опрос по доставке / Сводная информация',
                'filters' => $filters,
            ]) ?>
            <div class="search-row">
                <a href="#" class="unload-wrap">
                    <div class="icon">
                    </div>
                    <div class="text">
                        <div class="big-text">
                            Выгрузка
                        </div>
                        <div class="small-text">
                            Скачать файл
                        </div>
                    </div>
                </a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th rowspan="2" class="long-col">
                                Регион
                            </th>
                            <th rowspan="2" class="long-col">
                                Город
                            </th>
                            <th rowspan="2" class="long-col">
                                Завод
                            </th>
                            <th rowspan="2">
                                Отправлено<br>
                                приглашений
                            </th>
                            <th rowspan="2">
                                Количество <br>
                                заполненых<br>
                                анкет
                            </th>
                            <th rowspan="2">
                                Оценка<br>
                                NPS
                            </th>
                            <th rowspan="2" class="long-col">
                                Как вы оцениваете доставку товара?
                            </th>
                            <th rowspan="2" class="long-col">
                                Курьер приехал в согласованное время доставки?
                            </th>
                            <th rowspan="2" class="long-col">
                                Как вы оцениваете сохранность упаковки самого товара?
                            </th>
                            <th rowspan="2" class="long-col">
                                Курьер предупредил ли заранее о своем приезде?
                            </th>
                            <th rowspan="2">
                                Доля <br>
                                Алертов (%)
                            </th>
                            <th colspan="5" class="th-alert">
                                <div>
                                    Алерты
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th>Всего</th>
                            <th>
                                Не взят в <br>
                                работу (шт/%)
                            </th>
                            <th>
                                Не обработан <br>
                                в срок (шт/%)
                            </th>
                            <th>
                                В работе <br>
                                (шт/%)
                            </th>
                            <th>
                                Закрыто <br>
                                (шт/%)
                            </th>
                        </tr>
                        </thead>

                        <?php foreach ($data['items'] as $key1 => $item1): ?>
                            <tbody class="show-row">
                            <tr>
                                <td>
                                    <div class="open-next-row">
                                        <?= $key1 ?>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>

                                <td><?= $item1['data']['total'] ?></td>
                                <td><?= $item1['data']['filled'] ?></td>
                                <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['nps_average'] / $item1['data']['total'] : 0, 1, '.', '') ?></td>

                                <?php if (false): ?>
                                    <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['question1'] / $item1['data']['total'] : 0, 1, '.', '') ?></td>
                                    <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['question2'] / $item1['data']['total'] : 0, 1, '.', '') ?></td>
                                    <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['question3'] / $item1['data']['total'] : 0, 1, '.', '') ?></td>
                                    <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['question4'] / $item1['data']['total'] : 0, 1, '.', '') ?></td>
                                <?php endif; ?>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['alert'] / $item1['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                <td><?= $item1['data']['alert'] ?></td>
                                <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['status_new'] / $item1['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['status_expired'] / $item1['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['status_taken'] / $item1['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                <td><?= number_format($item1['data']['total'] != 0 ? $item1['data']['status_closed'] / $item1['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                            </tr>
                            </tbody>

                            <?php foreach ($data['items'][$key1]['items'] as $key2 => $item2): ?>
                                <tbody class="hidden-row-1">
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="open-next-row-2">
                                            <?= $key2 ?>
                                        </div>
                                    </td>
                                    <td></td>

                                    <td><?= $item2['data']['total'] ?></td>
                                    <td><?= $item2['data']['filled'] ?></td>
                                    <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['nps_average'] / $item2['data']['total'] : 0, 1, '.', '') ?></td>

                                    <?php if (false): ?>
                                        <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['question1'] / $item2['data']['total'] : 0, 1, '.', '') ?></td>
                                        <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['question2'] / $item2['data']['total'] : 0, 1, '.', '') ?></td>
                                        <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['question3'] / $item2['data']['total'] : 0, 1, '.', '') ?></td>
                                        <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['question4'] / $item2['data']['total'] : 0, 1, '.', '') ?></td>
                                    <?php endif; ?>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['alert'] / $item2['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                    <td><?= $item2['data']['alert'] ?></td>
                                    <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['status_new'] / $item2['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                    <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['status_expired'] / $item2['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                    <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['status_taken'] / $item2['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                    <td><?= number_format($item2['data']['total'] != 0 ? $item2['data']['status_closed'] / $item2['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                </tr>
                                </tbody>

                                <tbody class="hidden-row-2">
                                <?php foreach ($data['items'][$key1]['items'][$key2]['items'] as $key3 => $item3): ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><?= $key3 ?></td>

                                        <td><?= $item3['data']['total'] ?></td>
                                        <td><?= $item3['data']['filled'] ?></td>
                                        <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['nps_average'] / $item3['data']['total'] : 0, 1, '.', '') ?></td>

                                        <?php if (false): ?>
                                            <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['question1'] / $item3['data']['total'] : 0, 1, '.', '') ?></td>
                                            <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['question2'] / $item3['data']['total'] : 0, 1, '.', '') ?></td>
                                            <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['question3'] / $item3['data']['total'] : 0, 1, '.', '') ?></td>
                                            <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['question4'] / $item3['data']['total'] : 0, 1, '.', '') ?></td>
                                        <?php endif; ?>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>

                                        <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['alert'] / $item3['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                        <td><?= $item3['data']['alert'] ?></td>
                                        <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['status_new'] / $item3['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                        <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['status_expired'] / $item3['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                        <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['status_taken'] / $item3['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                        <td><?= number_format($item3['data']['total'] != 0 ? $item3['data']['status_closed'] / $item3['data']['total'] * 100 : 0, 1, '.', '') ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
