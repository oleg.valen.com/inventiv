<?php

use yii\helpers\Html;

?>

<div class="page-wrapper">
    <div class="page-inner">
        <div class="top-panel">
            <div class="logo">
                <?= Html::img('@web/img/logo.png', ['alt' => 'reStore']) ?>
            </div>
        </div>
        <div class="thanks2-content">
            <div class="thanks-pink anket-done-pink">
                <div class="text"></div>
                <div class="big-text">
                    Спасибо!
                </div>
                <div class="text">
                    Вы уже приняли участие в опросе.
                </div>
            </div>
        </div>
    </div>
</div>
