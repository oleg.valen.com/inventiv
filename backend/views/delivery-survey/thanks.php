<?php

use yii\helpers\Html;

?>

<div class="page-wrapper">
    <div class="page-inner">
        <div class="top-panel">
            <div class="logo">
                <?= Html::img('@web/img/logo.png', ['alt' => 'reStore']) ?>
            </div>
        </div>
        <div class="thanks-content">
            <div class="thanks-block">
                <div class="picture">
                    <?= Html::img('@web/img/icon-like.png', ['alt' => 'Like']) ?>
                </div>
                <div class="text">
                    <?= $username ?>, большое спасибо за Ваши ответы. Ваша обратная связь позволит нам стать
                    лучше!
                </div>
            </div>
        </div>
    </div>
</div>