<?php

use yii\helpers\Html;

//use yii\bootstrap4\ActiveForm;
use backend\views\widgets\UserFilterWidget;
use yii\helpers\Url;
use common\models\User;
use common\models\DeliverySurvey;
use yii\widgets\ActiveForm;
use common\models\Log;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= UserFilterWidget::widget([
                'deliveryDoc' => $deliveryDoc,
                'title' => 'Опрос b2b / Все анкеты / Профиль клиента',
            ]) ?>
            <div class="client-answer-container">
                <div class="answer-item">
                    <span class="q-text">Таймер</span>
                    <div class="answer-box">
                        <div class="time-interval" data-status-updated-at="<?= $model->status_updated_at ?>"></div>
                    </div>
                </div>
                <p class="answer-title">Ответы клиента</p>
                <div class="answer-item no-border">
                    <span class="q-text">NPS</span>
                    <div class="answer-box">
                        <div class="point-square">
                            <span class="point"><?= $model->nps ?></span>
                        </div>
                        <div class="line"></div>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Как вы оцениваете доставку товара?</span>
                    <div class="answer-box">
                        <p class="answer-txt"><?= array_key_exists($model->question1, DeliverySurvey::$values['question1'])
                                ? DeliverySurvey::$values['question1'][$model->question1] : '' ?></p>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Курьер приехал в согласованное время доставки?</span>
                    <div class="answer-box">
                        <p class="answer-txt"><?= array_key_exists($model->question2, DeliverySurvey::$values['question2'])
                                ? DeliverySurvey::$values['question2'][$model->question2] : '' ?></p>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Как вы оцениваете сохранность упаковки самого товара?</span>
                    <div class="answer-box">
                        <p class="answer-txt"><?= array_key_exists($model->question3, DeliverySurvey::$values['question3'])
                                ? DeliverySurvey::$values['question3'][$model->question3] : '' ?></p>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Курьер предупредил ли заранее о своем приезде?</span>
                    <div class="answer-box">
                        <p class="answer-txt"><?= array_key_exists($model->question4, DeliverySurvey::$values['question4'])
                                ? DeliverySurvey::$values['question4'][$model->question4] : '' ?></p>
                    </div>
                </div>
                <div class="answer-item">
                    <span class="q-text">Комментарий</span>
                    <div class="answer-box">
                        <p class="answer-txt"><?= Html::encode($model->comment) ?></p>
                    </div>
                </div>
                <?php if (false): ?>
                    <?php if ($model->status == DeliverySurvey::STATUS_TAKEN || $model->status == DeliverySurvey::STATUS_ACCEPTED || $model->status == DeliverySurvey::STATUS_REVISION): ?>
                        <div class="answer-item">
                            <span class="q-text">Комментарий 2-го уровня</span>
                            <div class="answer-box">
                                <p class="answer-txt"><?= Html::encode($model->revision_comment) ?></p>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'delivery-update-form',
            ]) ?>
            <?php if ($logs): ?>
                <div class="alert-history-container">
                    <p class="answer-title">История обработки Алерта</p>
                    <?php foreach ($logs as $item): ?>
                        <div class="history-item">
                            <p class="history-item-name">Время события:</p>
                            <p class="history-item-data"><?= date('d.m.Y H:m:s', $item->created_at) ?></p>
                        </div>
                        <div class="history-item">
                            <p class="history-item-name">ФИО:</p>
                            <p class="history-item-data"><?= $item->user->surname_name ?></p>
                        </div>
                        <div class="history-item">
                            <p class="history-item-name">Действие:</p>
                            <p class="history-item-data"><?= Log::$statuses[$item->status] ?></p>
                        </div>
                        <?php if (!empty($item->comment)): ?>
                            <div class="history-item">
                                <p class="history-item-name">Комментарий о выполненой работе:</p>
                                <p class="history-item-data alert-comment"><?= $item->comment ?></p>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if (Yii::$app->user->can('viewLogistics')): ?>
                <?php if ($model->status == DeliverySurvey::STATUS_NEW
                    || $model->status == DeliverySurvey::STATUS_TAKEN
                    || $model->status == DeliverySurvey::STATUS_EXPIRED
                    || $model->status == DeliverySurvey::STATUS_TAKEN
                    || $model->status == DeliverySurvey::STATUS_NOT_CLOSED
                    || $model->status == DeliverySurvey::STATUS_NOT_CLOSED_FINAL
                ): ?>
                    <?php if ($model->status == DeliverySurvey::STATUS_NEW
                        || $model->status == DeliverySurvey::STATUS_EXPIRED
                        || $model->status == DeliverySurvey::STATUS_NOT_CLOSED
                        || $model->status == DeliverySurvey::STATUS_NOT_CLOSED_FINAL
                    ): ?>
                        <div class="btn-wrapper take-to-work">
                            <button type="button" class="anket-btn">Взять в работу</button>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->status == DeliverySurvey::STATUS_TAKEN): ?>
                        <div class="btn-wrapper complete-btn-wrapper" style="display: block">
                            <button type="button" class="anket-btn">Завершить выполнение</button>
                        </div>
                        <div class="comment-container">
                            <p>Комментарий сотрудника</p>
                            <textarea class="comment-field" name="comment" id="delivery-update-form"></textarea>
                            <span class="comment-error">Поле обязательно к заполнению</span>
                            <div class="send-btn-wrp">
                                <button type="submit" class="send-btn">Отправить</button>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (Yii::$app->user->getIdentity()->can(2) && $model->status == DeliverySurvey::STATUS_CONFIRMATION): ?>
                <div class="comment-container" style="display: block">
                    <div class="btns-row">
                        <div class="btn-wrapper accepted" data-mode="accepted">
                            <button type="button" class="anket-btn">Принять</button>
                        </div>
                        <div class="btn-wrapper revision" data-mode="revision">
                            <button type="button" class="anket-btn">На доработку</button>
                        </div>
                    </div>
                    <div class="revision-comment" style="display: none">
                        <p>Комментарий</p>
                        <textarea class="comment-field" name="revision-comment" id="delivery-update-form"></textarea>
                    </div>
                    <div class="btn-wrapper confirmation-send">
                        <button type="button" class="anket-btn">Отправить</button>
                    </div>
                </div>
            <?php endif; ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>