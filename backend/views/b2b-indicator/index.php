<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\views\widgets\B2bFilterWidget;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content bar-charts-content">
            <?= B2bFilterWidget::widget([
                'title' => 'Опрос b2b / Ключевые показатели',
            ]) ?>
            <div class="box-wrapper nps-row">
                <h3>
                    <span class="bold">NPS</span> Какая вероятность того, что вы порекомендуете друзьям и знакомым
                    совершить покупку техники в магазине
                </h3>
                <div class="main-nps">
                    <div>
                        <table class="block-table table-striped" id="faceectable">
                            <tbody>
                            <tr>
                                <td class="cntr_num" colspan="7">
                                    DETRACTORS
                                </td>
                                <td class="cntr_num" colspan="2">
                                    PASSIVES
                                </td>
                                <td class="cntr_num" colspan="2">
                                    PROMOTERS
                                </td>
                            </tr>
                            <tr>
                                <td class="cntr_num" colspan="7">
                                    <div class="planka-long"></div>
                                </td>
                                <td class="cntr_num" colspan="2">
                                    <div class="planka-long"></div>
                                </td>
                                <td class="cntr_num" colspan="2">
                                    <div class="planka-long"></div>
                                </td>
                            </tr>
                            <tr class="rate-th">
                                <td>
                                    <div class="rate-item">0</div>
                                </td>
                                <td>
                                    <div class="rate-item">1</div>
                                </td>
                                <td>
                                    <div class="rate-item">2</div>
                                </td>
                                <td>
                                    <div class="rate-item">3</div>
                                </td>
                                <td>
                                    <div class="rate-item">4</div>
                                </td>
                                <td>
                                    <div class="rate-item">5</div>
                                </td>
                                <td>
                                    <div class="rate-item">6</div>
                                </td>
                                <td>
                                    <div class="rate-item">7</div>
                                </td>
                                <td>
                                    <div class="rate-item">8</div>
                                </td>
                                <td>
                                    <div class="rate-item">9</div>
                                </td>
                                <td>
                                    <div class="rate-item">10</div>
                                </td>
                            </tr>
                            <tr class="dig-td">
                                <td>
                                    <p class="sum-bot">24</p>
                                </td>
                                <td>
                                    <p class="sum-bot">43</p>
                                </td>
                                <td>
                                    <p class="sum-bot">1</p>
                                </td>
                                <td>
                                    <p class="sum-bot">32</p>
                                </td>
                                <td>
                                    <p class="sum-bot">52</p>
                                </td>
                                <td>
                                    <p class="sum-bot">23</p>
                                </td>
                                <td>
                                    <p class="sum-bot">43</p>
                                </td>
                                <td>
                                    <p class="sum-bot">22</p>
                                </td>
                                <td>
                                    <p class="sum-bot">23</p>
                                </td>
                                <td>
                                    <p class="sum-bot">331</p>
                                </td>
                                <td>
                                    <p class="sum-bot">439</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="left-block">
                        <div class="promot-wrap">
                            <div class="label-promot">
                                <div class="table-txt11-sp">35</div>
                                <div class="table-txt12-sp">Promoters</div>
                                <div class="table-txt13-sp">45.5%</div>
                            </div>
                            <div class="label-promot">
                                <div class="table-txt11-sp">77</div>
                                <div class="table-txt12-sp">Passives</div>
                                <div class="table-txt13-sp">33.5%</div>
                            </div>
                            <div class="label-promot">
                                <div class="table-txt11-sp">13</div>
                                <div class="table-txt12-sp">Detractors</div>
                                <div class="table-txt13-sp">21.0%</div>
                            </div>
                        </div>
                        <div class="block-circle">
                            <div class="circle">
                                <span>Total</span>
                                <span>36.8%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-row">
                <div class="box-wrapper box-chart">
                    <h3>
                        Насколько было удобно и комфортно совершить покупку
                    </h3>
                    <div class="chart-content">
                        <div class="pieRound smalllegend">
                            <div class="chart">
                                <div class="chart-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-wrapper box-chart">
                    <h3>
                        Насколько готовы снова совершить покупки
                    </h3>
                    <div class="chart-content">
                        <div class="pieRound smalllegend">
                            <div class="chart">
                                <div class="chart-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//data for сhart
let chartData = [
    {
        element: '.chart-1', // класс елемента
        type: 'pieRound',               //тип графіка
        innerText: 'CSI',
        innerValue: 8.75,
        data: [                          //данные графика
            {
                background: "#B2FDBA",
                gradient: ['#B3FFBB', '#ABEFB2'],
                progress: "35",
                labelText: "Оценка 9 - 10"
            },
            {
                background: "#FFE09A",
                gradient: ['#FFE6AA', '#FFD070'],
                progress: "20",
                labelText: "Оценка 7 - 8"
            },
            {
                background: "#FEC1C1",
                gradient: ['#FEC4C4', '#FFAAAA'],
                progress: "45",
                labelText: "Оценка 0 - 6"
            },
        ]
    },
    {
        element: '.chart-2', // класс елемента
        type: 'pieRound',               //тип графіка
        innerText: 'CSI',
        innerValue: 8.75,
        data: [                          //данные графика
            {
                background: "#B2FDBA",
                gradient: ['#a7f8d0', '#b2f1d1'],
                progress: "40",
                labelText: "Оценка 9 - 10"
            },
            {
                background: "#FFE09A",
                gradient: ['#fbb185', '#FFF6AA'],
                progress: "22",
                labelText: "Оценка 7 - 8"
            },
            {
                background: "#FEC1C1",
                gradient: ['#fba8bc', '#ffb5cf'],
                progress: "60",
                labelText: "Оценка 0 - 6"
            },
        ]
    },
];
</script>