<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\views\widgets\AscFilterWidget;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content bar-charts-content">
            <?= AscFilterWidget::widget([
                'title' => 'Опрос АСЦ / Ключевые показатели',
            ]) ?>
            <div class="box-wrapper nps-row">
                <h3>
                    <span class="bold">NPS</span> Какая вероятность того, что вы порекомендуете друзьям и знакомым
                    совершить покупку техники в магазине
                </h3>
                <div class="main-nps">
                    <div>
                        <table class="block-table table-striped" id="faceectable">
                            <tbody>
                            <tr>
                                <td class="cntr_num" colspan="7">
                                    DETRACTORS
                                </td>
                                <td class="cntr_num" colspan="2">
                                    PASSIVES
                                </td>
                                <td class="cntr_num" colspan="2">
                                    PROMOTERS
                                </td>
                            </tr>
                            <tr>
                                <td class="cntr_num" colspan="7">
                                    <div class="planka-long"></div>
                                </td>
                                <td class="cntr_num" colspan="2">
                                    <div class="planka-long"></div>
                                </td>
                                <td class="cntr_num" colspan="2">
                                    <div class="planka-long"></div>
                                </td>
                            </tr>
                            <tr class="rate-th">
                                <td>
                                    <div class="rate-item">0</div>
                                </td>
                                <td>
                                    <div class="rate-item">1</div>
                                </td>
                                <td>
                                    <div class="rate-item">2</div>
                                </td>
                                <td>
                                    <div class="rate-item">3</div>
                                </td>
                                <td>
                                    <div class="rate-item">4</div>
                                </td>
                                <td>
                                    <div class="rate-item">5</div>
                                </td>
                                <td>
                                    <div class="rate-item">6</div>
                                </td>
                                <td>
                                    <div class="rate-item">7</div>
                                </td>
                                <td>
                                    <div class="rate-item">8</div>
                                </td>
                                <td>
                                    <div class="rate-item">9</div>
                                </td>
                                <td>
                                    <div class="rate-item">10</div>
                                </td>
                            </tr>
                            <tr class="dig-td">
                                <td>
                                    <p class="sum-bot">24</p>
                                </td>
                                <td>
                                    <p class="sum-bot">43</p>
                                </td>
                                <td>
                                    <p class="sum-bot">1</p>
                                </td>
                                <td>
                                    <p class="sum-bot">32</p>
                                </td>
                                <td>
                                    <p class="sum-bot">52</p>
                                </td>
                                <td>
                                    <p class="sum-bot">23</p>
                                </td>
                                <td>
                                    <p class="sum-bot">43</p>
                                </td>
                                <td>
                                    <p class="sum-bot">22</p>
                                </td>
                                <td>
                                    <p class="sum-bot">23</p>
                                </td>
                                <td>
                                    <p class="sum-bot">331</p>
                                </td>
                                <td>
                                    <p class="sum-bot">439</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="left-block">
                        <div class="promot-wrap">
                            <div class="label-promot">
                                <div class="table-txt11-sp">35</div>
                                <div class="table-txt12-sp">Promoters</div>
                                <div class="table-txt13-sp">40.5%</div>
                            </div>
                            <div class="label-promot">
                                <div class="table-txt11-sp">77</div>
                                <div class="table-txt12-sp">Passives</div>
                                <div class="table-txt13-sp">30.5%</div>
                            </div>
                            <div class="label-promot">
                                <div class="table-txt11-sp">13</div>
                                <div class="table-txt12-sp">Detractors</div>
                                <div class="table-txt13-sp">29.0%</div>
                            </div>
                        </div>
                        <div class="block-circle">
                            <div class="circle">
                                <span>Total</span>
                                <span>47%</span
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-row">
                <div class="box-wrapper box-chart">
                    <h3>
                        Как Вы оцениваете качество проведенных работ
                    </h3>
                    <div class="chart-content">
                        <div class="lines-chart">
                            <div class="chart">
                                <div class="chart-1">
                                    <div class="lineChartcont">
                                        <div class="lineChartlist">
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 56%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #FE91BB;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BC366A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Отлично</div>
                                                    <div class="value">45 шт. / 20%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 40%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #FE91BB;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BC366A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Хорошо</div>
                                                    <div class="value">32 шт. / 15%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 79%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #FE91BB;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BC366A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Удовлетворительно</div>
                                                    <div class="value">63 шт. / 29%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 50%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #FE91BB;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BC366A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Плохо</div>
                                                    <div class="value">40 шт. / 18%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 50%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #FE91BB;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BC366A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Очень плохо</div>
                                                    <div class="value">40 шт. / 18%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="y-axis">
                                            <div class="axis-item"> 0<br> шт</div>
                                            <div class="axis-item"> 10<br> шт</div>
                                            <div class="axis-item"> 20<br> шт</div>
                                            <div class="axis-item"> 30<br> шт</div>
                                            <div class="axis-item"> 40<br> шт</div>
                                            <div class="axis-item"> 50<br> шт</div>
                                            <div class="axis-item"> 60<br> шт</div>
                                            <div class="axis-item"> 70<br> шт</div>
                                            <div class="axis-item"> 80<br> шт</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-wrapper box-chart">
                    <h3>
                        Насколько время ремонта/обслуживания соответствует ожиданиям?
                    </h3>
                    <div class="chart-content">
                        <div class="lines-chart">
                            <div class="chart">
                                <div class="chart-2">
                                    <div class="lineChartcont">
                                        <div class="lineChartlist">
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 19%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #E1ADEA;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BB57CC;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Выше ожидаемого</div>
                                                    <div class="value">15 шт. / 9%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 88%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #E1ADEA;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BB57CC;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Соответствуют ожиданиям</div>
                                                    <div class="value">70 шт. / 42%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 38%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #E1ADEA;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BB57CC;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Затрудняюсь ответить</div>
                                                    <div class="value">30 шт. / 18%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 44%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #E1ADEA;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BB57CC;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Ниже ожидаемого</div>
                                                    <div class="value">35 шт. / 21%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 19%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #E1ADEA;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #BB57CC;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Существенно ниже ожидаемого</div>
                                                    <div class="value">15 шт. / 9%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="y-axis">
                                            <div class="axis-item"> 0<br> шт</div>
                                            <div class="axis-item"> 10<br> шт</div>
                                            <div class="axis-item"> 20<br> шт</div>
                                            <div class="axis-item"> 30<br> шт</div>
                                            <div class="axis-item"> 40<br> шт</div>
                                            <div class="axis-item"> 50<br> шт</div>
                                            <div class="axis-item"> 60<br> шт</div>
                                            <div class="axis-item"> 70<br> шт</div>
                                            <div class="axis-item"> 80<br> шт</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-row">
                <div class="box-wrapper box-chart">
                    <h3>
                        Насколько скорость ремонта/обслуживания соответствует ожиданиям?
                    </h3>
                    <div class="chart-content">
                        <div class="lines-chart">
                            <div class="chart">
                                <div class="chart-3">
                                    <div class="lineChartcont">
                                        <div class="lineChartlist">
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 33%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #BCF1A1;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #6BCF3A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Выше ожидаемого</div>
                                                    <div class="value">35 шт. / 22%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 10%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #BCF1A1;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #6BCF3A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Соответствуют ожиданиям</div>
                                                    <div class="value">10 шт. / 6%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 86%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #BCF1A1;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #6BCF3A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Затрудняюсь ответить</div>
                                                    <div class="value">90 шт. / 56%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 21%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #BCF1A1;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #6BCF3A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Ниже ожидаемого</div>
                                                    <div class="value">22 шт. / 14%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 3%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #BCF1A1;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #6BCF3A;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Существенно ниже ожидаемого</div>
                                                    <div class="value">3 шт. / 2%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="y-axis">
                                            <div class="axis-item"> 0<br> шт</div>
                                            <div class="axis-item"> 15<br> шт</div>
                                            <div class="axis-item"> 30<br> шт</div>
                                            <div class="axis-item"> 45<br> шт</div>
                                            <div class="axis-item"> 60<br> шт</div>
                                            <div class="axis-item"> 75<br> шт</div>
                                            <div class="axis-item"> 90<br> шт</div>
                                            <div class="axis-item"> 105<br> шт</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-wrapper box-chart">
                    <h3>
                        Насколько были удовлетворены посещением сервисного центра в целом?
                    </h3>
                    <div class="chart-content">
                        <div class="lines-chart">
                            <div class="chart">
                                <div class="chart-4">
                                    <div class="lineChartcont">
                                        <div class="lineChartlist">
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 81%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #A9A8FF;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #716EFE;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Очень удовлетворен</div>
                                                    <div class="value">65 шт. / 46%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 25%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #A9A8FF;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #716EFE;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Удовлетворен</div>
                                                    <div class="value">20 шт. / 14%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 41%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #A9A8FF;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #716EFE;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Затрудняюсь ответить</div>
                                                    <div class="value">33 шт. / 24%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 18%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #A9A8FF;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #716EFE;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Не удовлетворен</div>
                                                    <div class="value">14 шт. / 10%</div>
                                                </div>
                                            </div>
                                            <div class="lineChartRow">
                                                <div class="line-col">
                                                    <div class="line">
                                                        <div class="active-line"
                                                             style="width: 10%;background: linear-gradient(317.7deg, rgba(0, 0, 0, 0.4) 0%, rgba(255, 255, 255, 0.4) 105.18%), #A9A8FF;box-shadow: inset -2.5px -2.5px 5px rgba(250, 251, 255, 0.1), inset 2.5px 2.5px 5px #716EFE;"></div>
                                                    </div>
                                                </div>
                                                <div class="label-row">
                                                    <div class="label">Совсем не удовлетворен</div>
                                                    <div class="value">8 шт. / 6%</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="y-axis">
                                            <div class="axis-item"> 0<br> шт</div>
                                            <div class="axis-item"> 10<br> шт</div>
                                            <div class="axis-item"> 20<br> шт</div>
                                            <div class="axis-item"> 30<br> шт</div>
                                            <div class="axis-item"> 40<br> шт</div>
                                            <div class="axis-item"> 50<br> шт</div>
                                            <div class="axis-item"> 60<br> шт</div>
                                            <div class="axis-item"> 70<br> шт</div>
                                            <div class="axis-item"> 80<br> шт</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
