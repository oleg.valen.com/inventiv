<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\views\widgets\B2bFilterWidget;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= B2bFilterWidget::widget([
                'title' => 'Опрос b2b / Сводная информация',
            ]) ?>
            <div class="search-row">
                <a href="#" class="unload-wrap">
                    <div class="icon">
                    </div>
                    <div class="text">
                        <div class="big-text">
                            Выгрузка
                        </div>
                        <div class="small-text">
                            Скачать файл
                        </div>
                    </div>
                </a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive">
                    <table>
                        <thead>
                        <tr>
                            <th rowspan="2" class="long-col">
                                Компания
                            </th>
                            <th rowspan="2" class="long-col">
                                ID
                            </th>
                            <th rowspan="2">
                                Отправлено<br>
                                приглашений
                            </th>
                            <th rowspan="2">
                                Количество <br>
                                заполненых<br>
                                анкет
                            </th>
                            <th rowspan="2">
                                Оценка<br>
                                NPS
                            </th>
                            <th rowspan="2" class="long-col">
                                Насколько было <br>
                                удобно и комфортно <br>
                                совершить покупку
                            </th>
                            <th rowspan="2" class="long-col">
                                Насколько готовы <br>
                                снова совершить <br>
                                покупки
                            </th>
                            <th rowspan="2">
                                Доля <br>
                                Алертов (%)
                            </th>
                            <th colspan="5" class="th-alert">
                                <div>
                                    Алерты
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th>Всего</th>
                            <th>
                                Не взят в <br>
                                работу (шт/%)
                            </th>
                            <th>
                                Не обработан <br>
                                в срок (шт/%)
                            </th>
                            <th>
                                В работе <br>
                                (шт/%)
                            </th>
                            <th>
                                Закрыто <br>
                                (шт/%)
                            </th>
                        </tr>
                        </thead>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Компания 1
                                </div>
                            </td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                1251353
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                6345645
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Компания 2
                                </div>
                            </td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                1251353
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                6345645
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Компания 3
                                </div>
                            </td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                1251353
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                6345645
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="shown-row">
                        <tr>
                            <td>
                                <div class="open-next-row">
                                    Компания 4
                                </div>
                            </td>
                            <td></td>
                            <td>
                                8733
                            </td>
                            <td>
                                7236
                            </td>
                            <td>
                                9.1
                            </td>
                            <td></td>
                            <td></td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>9.1</td>
                            <td>1.8</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                1251353
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                        <tbody class="hidden-row-1">
                        <tr>
                            <td>
                            </td>
                            <td>
                                6345645
                            </td>
                            <td>
                                923
                            </td>
                            <td>
                                198
                            </td>
                            <td>
                                7,5
                            </td>

                            <td></td>
                            <td></td>

                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                            <td>7,5</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
