<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DeliverySurvey */

$this->title = $model->survey_id;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Surveys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="delivery-survey-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'survey_id' => $model->survey_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'survey_id' => $model->survey_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'survey_id',
            'client_id',
            'doc_id',
            'question1',
            'question2',
            'question3',
            'question4',
            'status',
            'created_at',
            'updated_at',
            'status_updated_at',
        ],
    ]) ?>

</div>