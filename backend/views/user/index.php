<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\views\widgets\UserFilterWidget;
use yii\helpers\Url;
use common\models\User;

?>

<div class="container">
    <div class="header">
        <div class="navbar-aside">
            <div class="mobile-menu btn-aside-menu toggle-sidebar">
                <div class="b-menu">
                    <div class="b-bun b-bun--top"></div>
                    <div class="b-bun b-bun--mid"></div>
                    <div class="b-bun b-bun--bottom"></div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('/site/sidebar', [
        'id' => $id,
    ]) ?>
    <div class="content" id="content">
        <div class="page-content">
            <?= UserFilterWidget::widget([
                'title' => 'Пользователи',
            ]) ?>
            <div class="users-btns-wrap">
                <a href="<?= Url::to(['user/create']) ?>" class="btn-user">Добавить пользователя</a>
                <a href="#" class="btn-user">Сбросить фильтры</a>
            </div>
            <div class="table-wrap">
                <div class="table-responsive users-table">
                    <table>
                        <thead>
                        <tr>
                            <th>
                                Логин
                            </th>
                            <th>
                                Пароль
                            </th>
                            <th>
                                ФИО сотрудника
                            </th>
                            <th>
                                Права доступа
                            </th>
                            <th class="long-col">
                                Для какого опроса
                            </th>
                            <th>
                                E-mail
                            </th>
                            <th>
                                Телефон
                            </th>
                            <th>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="filter-row">
                            <td>
                                <input type="text" placeholder="Введите Логин">
                            </td>
                            <td>
                                <input type="text" placeholder="Введите пароль">
                            </td>
                            <td>
                                <input type="text" placeholder="Фио сотрудника">
                            </td>
                            <td>
                                <select class="select2-single">
                                    <option value="0" selected>Все</option>
                                    <?php foreach (User::$access as $id => $item): ?>
                                        <option value="<?= $id ?>"><?= $item ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <select class="select2-single">
                                    <option value="0">Все</option>
                                    <?php foreach (User::$surveyType as $id => $item): ?>
                                        <option value="<?= $id ?>"><?= $item ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <input type="text" placeholder="Введите email">
                            </td>
                            <td>
                                <input type="text" placeholder="Введите телефон">
                            </td>
                            <td></td>
                        </tr>
                        <?php foreach ($users as $item): ?>
                            <tr>
                                <td><?= $item->username ?></td>
                                <td></td>
                                <td><?= $item->surname_name ?></td>
                                <td><?= array_key_exists($item->access, User::$access) ? User::$access[$item->access] : '' ?></td>
                                <td><?= array_key_exists($item->survey_type, User::$surveyType) ? User::$surveyType[$item->survey_type] : '' ?></td>
                                <td><?= $item->email ?></td>
                                <td><?= $item->phone ?></td>
                                <td>
                                    <div class="edit-wrap">
                                        <a href="<?= Url::to(['user/delete', 'id' => $item->id]) ?>" class="btn-delete"
                                           onclick="return confirm('Подтвердить удаление?')"></a>
                                        <a href="<?= Url::to(['user/update', 'id' => $item->id]) ?>"
                                           class="btn-edit"></a>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>