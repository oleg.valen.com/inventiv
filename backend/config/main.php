<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/index',
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => [/*'*', '127.0.0.1', '::1', */'217.66.97.137', '31.43.56.3']
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info', 'trace', 'profile'],
                    'categories' => ['api'],
                    'logFile' => '@app/runtime/logs/api.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'api',
                    'pluralize' => false,
                ],
                'count' => 'site/count',
                'index' => 'site/index',
                'payment' => 'site/payment',
                'payed' => 'site/payed',
                'bonus' => 'site/bonus',
                'accumulation' => 'site/accumulation',
                'test-calc' => 'site/test-calc',
                'charity' => 'site/charity',
                'excel/export-excel' => 'excel/export-excel',
                'excel/import-excel' => 'excel/import-excel',

                'retail-survey' => 'retail-survey/index',
                'retail-survey/<action:[\w-]+>' => 'retail-survey/<action>',
                'retail-table' => 'retail-table/index',

                'delivery-survey' => 'delivery-survey/index',
                'delivery-survey/<action:[\w-]+>' => 'delivery-survey/<action>',
                'delivery-indicator' => 'delivery-indicator/index',
                'delivery-table' => 'delivery-table/index',

                'b2b-survey' => 'b2b-survey/index',
                'b2b-survey/<action:[\w-]+>' => 'b2b-survey/<action>',
                'b2b-indicator' => 'b2b-indicator/index',
                'b2b-table' => 'b2b-table/index',

                'asc-survey' => 'asc-survey/index',
                'asc-survey/<action:[\w-]+>' => 'asc-survey/<action>',
                'asc-indicator' => 'asc-indicator/index',
                'asc-table' => 'asc-table/index',

                'user' => 'user/index',
                'user/<action:[\w-]+>' => 'user/<action>',

                '<action:[\w-]+>' => 'site/<action>',

                'admin/<controller:\w+>/<action:\w+>/' => 'admin/<controller>/<action>',
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
//                    'js' => ['/admin/js/jquery-3.5.1.min.js'],
//                    'js' => ['/admin/js/jquery.min.js'],
//                    'jsOptions' => ['position' => \yii\web\View::POS_HEAD],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => []
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
//            'linkAssets' => true,
        ],
    ],
    'params' => $params,
];
