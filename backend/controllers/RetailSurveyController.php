<?php

namespace backend\controllers;

use common\models\DeliverySurvey;
use common\models\DeliverySurveySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class RetailSurveyController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
//                    [
//                        'actions' => ['create', 'thanks', 'done'],
//                        'allow' => true,
//                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
        ]);
    }

    public function actionUpdate($survey_id)
    {
        $model = $this->findModel($survey_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'survey_id' => $model->$survey_id]);
        }

        return $this->render('update', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
            'deliveryDoc' => $model->deliveryDoc,
        ]);
    }

    protected function findModel($survey_id)
    {
        if (($model = DeliverySurvey::findOne($survey_id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist . ');
    }

}
