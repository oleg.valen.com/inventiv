<?php

namespace backend\controllers;

use backend\models\DeliverySurveyUploadForm;
use common\models\Area;
use common\models\Client;
use common\models\DeliveryDoc;
use common\models\DeliverySurvey;
use common\models\DeliverySurveySearch;
use common\models\ClientToken;
use common\models\Factory;
use common\models\TransportCompany;
use common\models\User;
use common\services\SmsService;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\web\UploadedFile;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create'],
                'rules' => [
//                    [
//                        'actions' => ['create'],
//                        'allow' => true,
//                    ],
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $users = User::find()->where(['!=', 'status', User::STATUS_DELETED])->all();

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'users' => $users,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;

        $model = new User();
        $model->status = User::STATUS_ACTIVE;
        if ($request->isPost) {
            if ($model->load($request->post()) && $model->validate()) {
                $model->status = array_key_exists('status', $this->request->post()['User']) ? User::STATUS_ACTIVE : User::STATUS_INACTIVE;
                $model->setPassword($this->request->post()['User']['password']);
                $model->save(false);
                $model->redirect(['/user']);
            }
        }

        return $this->render('create', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->status = array_key_exists('status', $this->request->post()['User']) ? User::STATUS_ACTIVE : User::STATUS_INACTIVE;
                $model->setPassword($this->request->post()['User']['password']);
                $model->save();
                return $this->redirect(['/user']);
            }
        }

        return $this->render('update', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = User::STATUS_DELETED;
        $model->save();

        return $this->redirect(['/user']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist . ');
    }

}
