<?php

namespace backend\controllers;

use backend\models\DeliverySurveyUploadForm;
use common\models\DeliverySurvey;
use common\models\DeliverySurveySearch;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use Yii;

class DeliveryIndicatorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
//                    [
//                        'actions' => ['create', 'thanks', 'done'],
//                        'allow' => true,
//                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $form = null;
        $uploadModel = new DeliverySurveyUploadForm();
        $questions = [
            'question1' => array_fill(1, 5, 0),
            'question2' => array_fill(1, 3, 0),
            'question3' => array_fill(1, 4, 0),
            'question4' => array_fill(1, 3, 0),
        ];
        $nps = [
            'grades' => array_fill(0, 11, 0),
            'detractors' => [0, 0],
            'passives' => [0, 0],
            'promoters' => [0, 0],
            'total' => 0,
        ];

        $query = DeliverySurvey::find()
            ->joinWith('deliveryDoc')
            ->where(['not', ['nps' => null]]);
        if ($request->isPost) {
            $form = $post['Form'];
            $dates = explode(' - ', $form['deliveryDate']);
            $date0 = $dates[0];
            $date1 = $dates[1];
            $query = $query->andWhere([
                'between',
                'delivery_survey.created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
            if ($form['transportCompanyId']) $query = $query->andWhere(['delivery_doc.transport_company_id' => $form['transportCompanyId']]);
            if ($form['statusDeliveryId']) $query = $query->andWhere(['delivery_doc.status_delivery_id' => $form['statusDeliveryId']]);
            if ($form['timeRangeName']) $query = $query->andWhere(['delivery_doc.time_range' => $form['timeRangeName']]);
            if ($form['factoryId']) $query = $query->andWhere(['delivery_doc.factory_id' => $form['factoryId']]);
            if ($form['areaId']) $query = $query->andWhere(['delivery_doc.area_id' => $form['areaId']]);
        } else {
            $date0 = date('01-m-Y');
            $date1 = date('d-m-Y', time());
            $query = $query->andWhere([
                'between',
                'delivery_survey.created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
        }

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $deliverySurveys = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['survey_id' => SORT_DESC])
            ->all();

        foreach ($queryAll = $countQuery->all() as $item) {
            $nps['grades'][$item->nps]++;
            if (in_array($item->nps, DeliverySurvey::BAD_NPS, true)) {
                $nps['detractors'][0]++;
            } elseif (in_array($item->nps, DeliverySurvey::PASSIVE_NPS, true)) {
                $nps['passives'][0]++;
            } elseif (in_array($item->nps, DeliverySurvey::GOOD_NPS, true)) {
                $nps['promoters'][0]++;
            }

            foreach (range(1, 4) as $i) {
                if ($item->{"question{$i}"} != null) {
                    $questions["question{$i}"][$item->{"question{$i}"}]++;
                }
            }
        }
        unset($item);

        $nps['detractors'][1] = count($queryAll) != 0 ? number_format($nps['detractors'][0] / count($queryAll) * 100, 2, '.', '') : 0;
        $nps['passives'][1] = count($queryAll) != 0 ? number_format($nps['passives'][0] / count($queryAll) * 100, 2, '.', '') : 0;
        $nps['promoters'][1] = count($queryAll) != 0 ? number_format($nps['promoters'][0] / count($queryAll) * 100, 2, '.', '') : 0;
        $nps['total'] = count($queryAll) != 0 ? round(($nps['promoters'][0] - $nps['detractors'][0]) / count($queryAll) * 100) : 0;

        foreach ($questions as &$question) {
            $k = floor(max($question) / 100) + 1;
            $scale = $k * 100;
            $step = $scale / 10;
            foreach (range(1, 9) as $i) {
                $question['scale'][] = $scale;
                $scale -= $step;
            }
            $question['k'] = $k;
        }
        unset($question, $item);

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'deliverySurveys' => $deliverySurveys,
            'uploadModel' => $uploadModel,
            'pages' => $pages,
            'filters' => [
                'transportCompanyId' => $form ? $form['transportCompanyId'] : '',
                'deliveryDate' => $date0 . ' - ' . $date1,
                'statusDeliveryId' => $form ? $form['statusDeliveryId'] : '',
                'timeRangeName' => $form ? $form['timeRangeName'] : '',
                'factoryId' => $form ? $form['factoryId'] : '',
                'areaId' => $form ? $form['areaId'] : '',
            ],
            'questions' => $questions,
            'nps' => $nps,
        ]);
    }

}
