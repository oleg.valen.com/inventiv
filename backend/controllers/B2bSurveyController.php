<?php

namespace backend\controllers;

use backend\models\DeliverySurveyUploadForm;
use common\models\Area;
use common\models\Client;
use common\models\DeliveryDoc;
use common\models\DeliverySurvey;
use common\models\DeliverySurveySearch;
use common\models\ClientToken;
use common\models\Factory;
use common\models\TransportCompany;
use common\services\SmsService;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\web\UploadedFile;

/**
 * DeliverySurveyController implements the CRUD actions for DeliverySurvey model.
 */
class B2bSurveyController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'thanks', 'done', 'upload'],
                'rules' => [
                    [
                        'actions' => ['create', 'thanks', 'done'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySurvey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $deliverySurveys = DeliverySurvey::find()->limit(20)->all();

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'deliverySurveys' => $deliverySurveys,
        ]);
    }

    /**
     * Displays a single DeliverySurvey model.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($survey_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($survey_id),
        ]);
    }

    /**
     * Creates a new DeliverySurvey model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($token)
    {
        $request = Yii::$app->request;

        $clientToken = ClientToken::find()->where(['token' => $token])->one();
        if ($token == null || $clientToken == null)
            throw new NotFoundHttpException('Unknown token.');

        $deliverySurvey = DeliverySurvey::find()->where(['doc_id' => $clientToken->doc_id])->one();
        if ($deliverySurvey->nps != null)
            $this->redirect(['done']);

        if ($request->isPost) {
            if ($deliverySurvey->load($request->post()) && $deliverySurvey->validate()) {

                if (in_array($deliverySurvey->nps, DeliverySurvey::BAD_NPS)) {
                    $deliverySurvey->status = DeliverySurvey::STATUS_NEW;
                }
                $deliverySurvey->save(false);
                $this->redirect(['thanks', 'username' => $deliverySurvey->client->name]);
            }
        }

        return $this->render('create', [
            'deliverySurvey' => $deliverySurvey,
            'values' => DeliverySurvey::$values,
        ]);
    }

    /**
     * Updates an existing DeliverySurvey model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($survey_id)
    {
        $model = $this->findModel($survey_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'survey_id' => $model->$survey_id]);
        }

        return $this->render('update', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
            'deliveryDoc' => $model->deliveryDoc,
        ]);
    }

    /**
     * Deletes an existing DeliverySurvey model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($survey_id)
    {
        $this->findModel($survey_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DeliverySurvey model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $survey_id Номер анкеты
     * @return DeliverySurvey the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($survey_id)
    {
        if (($model = DeliverySurvey::findOne($survey_id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist . ');
    }

    public function actionThanks()
    {
        return $this->render('thanks', [
            'username' => Yii::$app->request->get('username') ?? '',
        ]);
    }

    public function actionDone()
    {
        return $this->render('done');
    }

    public function actionUpload()
    {
        $areas = Area::find()->indexBy('name')->all();
        $factories = Factory::find()->indexBy('name')->all();
        $transportCompanies = TransportCompany::find()->indexBy('name')->all();

        $uploadModel = new DeliverySurveyUploadForm();
        if (Yii::$app->request->isPost) {
            $uploadModel->xlsxFile = UploadedFile::getInstance($uploadModel, 'xlsxFile');
            if ($path = $uploadModel->upload()) {
                if ($xlsx = \SimpleXLSX::parse($path)) {
                    $items = $xlsx->rows();
                } else {
                    echo SimpleXLSX::parseError();
                }
                foreach ($items as $key => $item) {
                    try {
                        if ($key == 0) continue;

                        if (array_key_exists($item[10], $areas)) {
                            $area = $areas[$item[10]];
                        } else {
                            $area = new Area(['name' => $item[10]]);
                            $area->save(false);
                            $areas[$area->name] = $area;
                        }

                        if (array_key_exists($item[8], $factories)) {
                            $factory = $factories[$item[8]];
                        } else {
                            $factory = new Factory(['name' => $item[8]]);
                            $factory->save(false);
                            $factories[$factory->name] = $factory;
                        }

                        if (array_key_exists($item[6], $transportCompanies)) {
                            $transportCompany = $transportCompanies[$item[6]];
                        } else {
                            $transportCompany = new TransportCompany(['name' => $item[6]]);
                            $transportCompany->save(false);
                            $transportCompanies[$transportCompany->name] = $transportCompany;
                        }

                        if (!$client = Client::find()->where(['phone' => $item[12]])->one()) {
                            $client = new Client(['phone' => $item[12]]);
                        }
                        $client->name = $item[14];
                        $client->surname = $item[15];
                        $client->email = $item[13];
                        $client->save(false);

                        if (!$deliveryDoc = DeliveryDoc::find()->where(['numdoc_1c' => $item[3]])->one()) {
                            $deliveryDoc = new DeliveryDoc(['numdoc_1c' => $item[3]]);
                        }

                        $deliveryDoc->client_id = $client->client_id;
                        $deliveryDoc->sum = $item[4];
                        $deliveryDoc->delivery_date = strtotime($item[5]);
                        $deliveryDoc->transport_company_id = $transportCompany->id;
                        $deliveryDoc->time_range = $item[7];
                        $deliveryDoc->factory_id = $factory->id;
                        $deliveryDoc->area_id = $area->id;
                        $deliveryDoc->save(false);
                        unset($item);

                    } catch (\Exception $e) {
//                        self::$errors[] = $e->getMessage();
//                        Yii::error([
//                            'name' => 'bonuseCalculation',
//                            'data' => ['date' => $date],
//                            'error' => $e->getMessage(),
//                        ], 'api');
                    }
                }
                unset($item);

                @unlink($path);
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }

}
