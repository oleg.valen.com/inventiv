<?php

namespace backend\controllers;

use backend\models\DeliverySurveyUploadForm;
use common\models\Area;
use common\models\City;
use common\models\Client;
use common\models\DeliveryDoc;
use common\models\DeliverySurvey;
use common\models\DeliverySurveySearch;
use common\models\ClientToken;
use common\models\DeliverySurveyStatus\DeliverySurveyContext;
use common\models\Factory;
use common\models\Log;
use common\models\StatusDelivery;
use common\models\TransportCompany;
use common\models\User;
use common\services\SmsService;
use console\controllers\CronController;
use console\services\CronService;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\UploadedFile;
use yii\data\Pagination;

/**
 * DeliverySurveyController implements the CRUD actions for DeliverySurvey model.
 */
class DeliverySurveyController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'thanks', 'done', 'upload', 'update'],
                'rules' => [
                    [
                        'actions' => ['create', 'thanks', 'done'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'upload', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySurvey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $form = null;
        $uploadModel = new DeliverySurveyUploadForm();
        $die = [
            'totalAll' => 0,
            'total' => 0,
            'badNps' => 0,
            'expired' => 0,
            'new' => 0,
            'taken' => 0,
        ];
        $session = Yii::$app->session;

        $query = DeliverySurvey::find()
            ->joinWith('deliveryDoc')
            ->where(['not', ['nps' => null]]);
        if ($request->isPost) {
            $form = $post['Form'];
            $dates = explode(' - ', $form['deliveryDate']);
            $date0 = $dates[0];
            $date1 = $dates[1];
            $query = $query->andWhere([
                'between',
                'delivery_survey.created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
            //todo переделать условия, фильтры по post, session
            if ($form['transportCompanyId']) {
                $query = $query->andWhere(['delivery_doc.transport_company_id' => $form['transportCompanyId']]);
                $session->set('transportCompanyId', $form['transportCompanyId']);
            }
            if ($form['statusDeliveryId']) {
                $query = $query->andWhere(['delivery_doc.status_delivery_id' => $form['statusDeliveryId']]);
                $session->set('statusDeliveryId', $form['statusDeliveryId']);
            }
            if ($form['timeRangeName']) {
                $query = $query->andWhere(['delivery_doc.time_range' => $form['timeRangeName']]);
                $session->set('timeRangeName', $form['timeRangeName']);
            }
            if ($form['factoryId']) {
                $query = $query->andWhere(['delivery_doc.factory_id' => $form['factoryId']]);
                $session->set('factoryId', $form['factoryId']);
            }
            if ($form['areaId']) {
                $query = $query->andWhere(['delivery_doc.area_id' => $form['areaId']]);
                $session->set('areaId', $form['areaId']);
            }
            if (isset($post['mode'])) {
                if ($post['mode'] == 'all') {
                    //affects only on numbers in dies (not refresh page)
                } elseif ($post['mode'] == 'bad-nps') {
                    $query = $query->andWhere(['in', 'delivery_survey.nps', DeliverySurvey::BAD_NPS]);
                } elseif ($post['mode'] == 'expired') {
                    $query = $query->andWhere(['delivery_survey.status' => DeliverySurvey::STATUS_NEW]);
                } elseif ($post['mode'] == 'new') {
                    $query = $query->andWhere(['delivery_survey.status' => DeliverySurvey::STATUS_NEW]);
                } elseif ($post['mode'] == 'taken') {
                    $query = $query->andWhere(['delivery_survey.status' => DeliverySurvey::STATUS_TAKEN]);
                }
            }
            $session->set('date0', $date0);
            $session->set('date1', $date1);
        } else {
            if ($session->has('transportCompanyId'))
                $query = $query->andWhere(['delivery_doc.transport_company_id' => $session->get('transportCompanyId')]);
            if ($session->has('statusDeliveryId'))
                $query = $query->andWhere(['delivery_doc.status_delivery_id' => $session->get('statusDeliveryId')]);
            if ($session->has('timeRangeName'))
                $query = $query->andWhere(['timeRangeName' => $session->get('timeRangeName')]);
            if ($session->has('factoryId'))
                $query = $query->andWhere(['delivery_doc.factory_id' => $session->get('factoryId')]);
            if ($session->has('areaId'))
                $query = $query->andWhere(['delivery_doc.area_id' => $session->get('areaId')]);

            $date0 = $session->get('date0', date('01-m-Y'));
            $date1 = $session->get('date1', date('d-m-Y', time()));
            $query = $query->andWhere([
                'between',
                'delivery_survey.created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);
        }

        $die['totalAll'] = DeliverySurvey::find()
            ->joinWith('deliveryDoc')
            ->where([
                'between',
                'delivery_survey.created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ])
            ->count();

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $deliverySurveys = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(['survey_id' => SORT_DESC])
            ->all();

        foreach ($countQuery->all() as $item) {
            $die['total']++;
            $die['badNps'] += in_array($item->nps, DeliverySurvey::BAD_NPS, true) ? 1 : 0;
            $die['expired'] += $item->status == DeliverySurvey::STATUS_EXPIRED ? 1 : 0;
            $die['new'] += $item->status == DeliverySurvey::STATUS_NEW ? 1 : 0;
            $die['taken'] += $item->status == DeliverySurvey::STATUS_TAKEN ? 1 : 0;
        }
        $die['badNps'] = round($die['total'] !== 0 ? ($die['badNps'] / $die['total'] * 100) : 0, 1);

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
            'deliverySurveys' => $deliverySurveys,
            'uploadModel' => $uploadModel,
            'pages' => $pages,
            'filters' => [
                'transportCompanyId' => $form ? $form['transportCompanyId'] : $session->has('transportCompanyId') ? $session->get('transportCompanyId') : '',
                'deliveryDate' => $date0 . ' - ' . $date1,
                'statusDeliveryId' => $form ? $form['statusDeliveryId'] : $session->has('statusDeliveryId') ? $session->get('statusDeliveryId') : '',
                'timeRangeName' => $form ? $form['timeRangeName'] : $session->has('timeRangeName') ? $session->get('timeRangeName') : '',
                'factoryId' => $form ? $form['factoryId'] : $session->has('factoryId') ? $session->get('factoryId') : '',
                'areaId' => $form ? $form['areaId'] : $session->has('areaId') ? $session->get('areaId') : '',
                'die' => $die,
            ],
        ]);
    }

    public function actionDeliverySurveyService()
    {

        $deliverySurveys = DeliverySurvey::find()
            ->where(['status' => DeliverySurvey::STATUS_INIT])
            ->andWhere(['in', 'nps', DeliverySurvey::BAD_NPS])
            ->andWhere(['not', ['nps' => null]])
            ->all();
        foreach ($deliverySurveys as $item) {
            $item->status = DeliverySurvey::STATUS_NEW;
            $item->save(false);
        }

//        $user = Yii::$app->user->getIdentity();
//        return in_array($user->id, Yii::$app->authManager->getUserIdsByRole('admin'))
//            || in_array($user->access, User::$levels[1]);

        //        Yii::info([
//            'name' => 'Start: ' . __METHOD__,
//            'time' => $start = microtime(true),
//        ], 'api');
//
//        try {
//            $cron = new CronService();
//            $cron->manageNotifications();
//        } catch (\Exception $e) {
//            $cron::$errors[] = $e->getMessage();
//            Yii::error([
//                'name' => 'api',
//                'error' => $e->getMessage()], 'api');
//        }
//
//        Yii::info([
//            'name' => 'Finish: ' . __METHOD__,
//            'time' => microtime(true) - $start,
//            'memory' => memory_get_usage(true),
//            'emailSentTotal' => CronService::$emailSentTotal,
//        ], 'api');

    }

    /**
     * Displays a single DeliverySurvey model.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($survey_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($survey_id),
        ]);
    }

    /**
     * Creates a new DeliverySurvey model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($token)
    {
        $request = Yii::$app->request;

        $clientToken = ClientToken::find()->where(['token' => $token])->one();
        if ($token == null || $clientToken == null)
            throw new NotFoundHttpException('Unknown token.');

        $deliverySurvey = DeliverySurvey::find()->where(['doc_id' => $clientToken->doc_id])->one();
        if ($deliverySurvey->nps != null)
            $this->redirect(['done']);

        if ($request->isPost) {
            if ($deliverySurvey->load($request->post()) && $deliverySurvey->validate()) {

                if (in_array($deliverySurvey->nps, DeliverySurvey::BAD_NPS)) {
                    $deliverySurvey->status = DeliverySurvey::STATUS_NEW;
                }
                if (!empty($deliverySurvey->comment)) {
                    $deliverySurvey->comment = Html::encode($deliverySurvey->comment);
                }
                $deliverySurvey->save(false);
                $this->redirect(['thanks', 'username' => $deliverySurvey->client->name]);
            }
        }

        return $this->render('create', [
            'deliverySurvey' => $deliverySurvey,
            'values' => DeliverySurvey::$values,
        ]);
    }

    /**
     * Updates an existing DeliverySurvey model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($survey_id)
    {
        $model = $this->findModel($survey_id);
        $request = Yii::$app->request;

        if ($request->isAjax) {
            if ($request->post('mode') == 'taken') {
                $model->status = DeliverySurvey::STATUS_TAKEN;
                $model->save(false);
                $log = new Log([
                    'type_id' => Log::TYPE_DELIVERY,
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'status' => DeliverySurvey::STATUS_TAKEN,
                ]);
                $log->save(false);
            } elseif ($request->post('mode') == 'accepted') {
                $model->status = DeliverySurvey::STATUS_ACCEPTED;
                $model->save(false);

                //todo проверить возможность удаления и удалить колонки в бд delivery_survey user_comment, revision_comment
                $log = new Log([
                    'type_id' => Log::TYPE_DELIVERY,
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'comment' => !empty($request->post('revision-comment')) ? Html::encode($request->post('revision-comment')) : null,
                    'status' => DeliverySurvey::STATUS_ACCEPTED,
                ]);
                $log->save(false);

                //todo сделать метод, чтобы уйти от повторяющегося кода - 3 строки
                $class = '\common\models\DeliverySurveyStatus\\' . DeliverySurvey::STATUS_CLASSES[$model->status];
                $deliverySurveysContext = new DeliverySurveyContext($model, new $class(['statusUpdatedAt' => $model->status_updated_at]));
                $deliverySurveysContext->handle();
            } elseif ($request->post('mode') == 'revision') {
                $model->status = DeliverySurvey::STATUS_REVISION;
                $model->save(false);

                $log = new Log([
                    'type_id' => Log::TYPE_DELIVERY,
                    'survey_id' => $survey_id,
                    'user_id' => Yii::$app->user->id,
                    'comment' => !empty($request->post('revision-comment')) ? Html::encode($request->post('revision-comment')) : null,
                    'status' => DeliverySurvey::STATUS_REVISION,
                ]);
                $log->save(false);

                $class = '\common\models\DeliverySurveyStatus\\' . DeliverySurvey::STATUS_CLASSES[$model->status];
                $deliverySurveysContext = new DeliverySurveyContext($model, new $class(['statusUpdatedAt' => $model->status_updated_at]));
                $deliverySurveysContext->handle();
            }
            return;
        }

        if ($this->request->isPost) {
            $model->status = DeliverySurvey::STATUS_CLOSED;
            $model->save(false);
            $log = new Log([
                'type_id' => Log::TYPE_DELIVERY,
                'survey_id' => $survey_id,
                'user_id' => Yii::$app->user->id,
                'comment' => !empty($request->post('comment')) ? Html::encode($request->post('comment')) : null,
                'status' => DeliverySurvey::STATUS_CLOSED,
            ]);
            $log->save(false);

            $class = '\common\models\DeliverySurveyStatus\\' . DeliverySurvey::STATUS_CLASSES[$model->status];
            $deliverySurveysContext = new DeliverySurveyContext($model, new $class(['statusUpdatedAt' => $model->status_updated_at]));
            $deliverySurveysContext->handle();

            return $this->redirect(['update', 'survey_id' => $survey_id]);
        }

        return $this->render('update', [
            'id' => Yii::$app->controller->id,
            'model' => $model,
            'deliveryDoc' => $model->deliveryDoc,
            'logs' => $model->logs,
        ]);
    }

    /**
     * Deletes an existing DeliverySurvey model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $survey_id Номер анкеты
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($survey_id)
    {
        $this->findModel($survey_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DeliverySurvey model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $survey_id Номер анкеты
     * @return DeliverySurvey the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($survey_id)
    {
        if (($model = DeliverySurvey::findOne($survey_id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist . ');
    }

    public function actionThanks()
    {
        return $this->render('thanks', [
            'username' => Yii::$app->request->get('username') ?? '',
        ]);
    }

    public function actionDone()
    {
        return $this->render('done');
    }

    public function actionUpload()
    {
        ini_set("memory_limit", "2048M");
        ini_set('max_execution_time', 2400);

        $areas = Area::find()->indexBy('name')->all();
        $cities = City::find()->indexBy('name')->all();
        $factories = Factory::find()->indexBy('name')->all();
        $transportCompanies = TransportCompany::find()->indexBy('name')->all();
        $statusDeliveries = StatusDelivery::find()->indexBy('name')->all();

        $uploadModel = new DeliverySurveyUploadForm();
        if (Yii::$app->request->isPost) {
            $uploadModel->xlsxFile = UploadedFile::getInstance($uploadModel, 'xlsxFile');
            if ($path = $uploadModel->upload()) {
                if ($xlsx = \SimpleXLSX::parse($path)) {
                    $items = $xlsx->rows();
                } else {
                    echo SimpleXLSX::parseError();
                }
                foreach ($items as $key => $item) {
                    try {
                        if ($key == 0) continue;

                        if (array_key_exists($item[11], $areas)) {
                            $area = $areas[$item[11]];
                        } else {
                            $area = new Area(['name' => $item[11]]);
                            $area->save(false);
                            $areas[$area->name] = $area;
                        }

                        if (array_key_exists($item[12], $cities)) {
                            $city = $cities[$item[12]];
                        } else {
                            $city = new City(['name' => $item[12]]);
                            $city->save(false);
                            $cities[$city->name] = $city;
                        }

                        if (array_key_exists($item[9], $factories)) {
                            $factory = $factories[$item[9]];
                        } else {
                            $factory = new Factory(['name' => $item[9]]);
                            $factory->save(false);
                            $factories[$factory->name] = $factory;
                        }

                        if (array_key_exists($item[7], $transportCompanies)) {
                            $transportCompany = $transportCompanies[$item[7]];
                        } else {
                            $transportCompany = new TransportCompany(['name' => $item[7]]);
                            $transportCompany->save(false);
                            $transportCompanies[$transportCompany->name] = $transportCompany;
                        }

                        if (array_key_exists($item[6], $statusDeliveries)) {
                            $statusDelivery = $statusDeliveries[$item[6]];
                        } else {
                            $statusDelivery = new StatusDelivery(['name' => $item[6]]);
                            $statusDelivery->save(false);
                            $statusDeliveries[$statusDelivery->name] = $statusDelivery;
                        }

                        if (!$client = Client::find()->where(['phone' => $item[13]])->one()) {
                            $client = new Client(['phone' => $item[13]]);
                        }
                        $client->name = $item[15];
                        $client->surname = $item[16];
                        $client->email = $item[14];
                        $client->save(false);

                        $deliveryDoc = DeliveryDoc::find()
                            ->where(['numdoc_1c' => $item[3]])
                            ->orderBy(['doc_id' => SORT_DESC])
                            ->one();
                        if ($deliveryDoc != null && (date('Y.m.d', $deliveryDoc->deliverySurvey->created_at) == date('Y.m.d', time()))) {

                        } else {
                            $deliveryDoc = new DeliveryDoc(['numdoc_1c' => $item[3]]);
                        }

                        $deliveryDoc->client_id = $client->client_id;
                        $deliveryDoc->numdoc_crm = $item[2];
                        $deliveryDoc->sum = $item[4];
                        $deliveryDoc->delivery_date = strtotime($item[5]);
                        $deliveryDoc->transport_company_id = $transportCompany->id;
                        $deliveryDoc->status_delivery_id = $statusDelivery->id;
                        $deliveryDoc->time_range = $item[8];
                        $deliveryDoc->factory_id = $factory->id;
                        $deliveryDoc->area_id = $area->id;
                        $deliveryDoc->city_id = $city->id;
                        $deliveryDoc->save(false);

                        unset($item);

                    } catch (\Exception $e) {
//                        self::$errors[] = $e->getMessage();
//                        Yii::error([
//                            'name' => 'bonuseCalculation',
//                            'data' => ['date' => $date],
//                            'error' => $e->getMessage(),
//                        ], 'api');
                    }
                }
                unset($item);

                $this->actionDeliverySurveyCreate();

                @unlink($path);
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDeliverySurveyCreate()
    {
        $deliveryDocs = DeliveryDoc::find()
            ->leftJoin('delivery_survey', '`delivery_doc`.`doc_id` = `delivery_survey`.`doc_id`')
            ->where(['is', 'delivery_survey.survey_id', null])
            ->all();
        foreach ($deliveryDocs as $item) {
            if ($item->deliverySurvey == null) {
                $deliverySurvey = new DeliverySurvey();
                $deliverySurvey->client_id = $item->client_id;
                $deliverySurvey->doc_id = $item->doc_id;
                $deliverySurvey->save(false);
            }
        }
    }

    public function actionSendSms()
    {
        $deliverySurveys = DeliverySurvey::find()
            ->where(['sms_sent' => null])
            ->all();
        foreach ($deliverySurveys as $item) {
            $token = new ClientToken();
            $token->client_id = $item->client->client_id;
            $token->doc_id = $item->doc_id;
            $token->generateToken();
            $token->save(false);

            $sms = new SmsService(Yii::$app->request->hostInfo . '/delivery/' . $token->token);
            $sms->send($item);
        }
        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUploadXlsx()
    {

        $request = Yii::$app->request;
        $post = $request->post();
        $form = null;
        $n = 0;

        include_once("xlsxwriter.class.php");
        $filename = "Выгрузка.xlsx";
        header('Content-disposition: attachment; filename="' . \XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $rows = [[
            'N п/п',
            '№ документа CRM из 1C',
            'Название почтового региона',
            'Временной диапазон доставки',
            'Дата доставки',
            'Наименование транспортной компании',
            'Статус доставки',
            'Дата опроса',
            'Телефон',
            'Статус SMS',
            'NPS',
            'Переход на анкету',
            'Как вы оцениваете доставку товара?',
            'Курьер приехал в согласованное время доставки?',
            'Как вы оцениваете сохранность упаковки самого товара?',
            'Курьер предупредил ли заранее о своем приезде?',
            'Комментарий клиента',
            'Статус алерта',
            'Номер',
        ]];

        $query = DeliverySurvey::find()
            ->joinWith('deliveryDoc')
            ->where(['not', ['nps' => null]]);

        $form = $post['Form'];
        $dates = explode(' - ', $form['deliveryDate']);
        $date0 = $dates[0];
        $date1 = $dates[1];

        $query = $query
            ->andWhere([
                'between',
                'delivery_survey.created_at',
                strtotime($date0), strtotime('tomorrow', strtotime($date1)) - 1
            ]);

        if ($form['transportCompanyId']) {
            $query = $query->andWhere(['delivery_doc.transport_company_id' => $form['transportCompanyId']]);
        }
        if ($form['statusDeliveryId']) {
            $query = $query->andWhere(['delivery_doc.status_delivery_id' => $form['statusDeliveryId']]);
        }
        if ($form['timeRangeName']) {
            $query = $query->andWhere(['delivery_doc.time_range' => $form['timeRangeName']]);
        }
        if ($form['factoryId']) {
            $query = $query->andWhere(['delivery_doc.factory_id' => $form['factoryId']]);
        }
        if ($form['areaId']) {
            $query = $query->andWhere(['delivery_doc.area_id' => $form['areaId']]);
        }
        $query = $query->all();

        foreach ($query as $item) {
            $deliveryDoc = $item->deliveryDoc;
            $rows[] = [
                ++$n,
                $item->deliveryDoc->numdoc_crm,
                $item->deliveryDoc->area->name,
                $item->deliveryDoc->time_range,
                date('d.m.Y h:m', $deliveryDoc->delivery_date),
                $deliveryDoc->transportCompany->name,
                $deliveryDoc->statusDelivery->name,
                date('d.m.Y h:m', $item->created_at),
                $deliveryDoc->client->phone,
                $item->sms_sent ? 'Доставлено' : 'Не доставлено',
                $item->nps,
                $item->question1 != null ? 'Да' : '',
                $item->question1 != null ? DeliverySurvey::$values['question1'][$item->question1] : '',
                $item->question2 != null ? DeliverySurvey::$values['question2'][$item->question2] : '',
                $item->question3 != null ? DeliverySurvey::$values['question3'][$item->question3] : '',
                $item->question4 != null ? DeliverySurvey::$values['question4'][$item->question4] : '',
                $item->comment,
                in_array($item->nps, DeliverySurvey::BAD_NPS, true) ? 'Новый' : '',
                in_array($item->nps, DeliverySurvey::BAD_NPS, true) ? $item->survey_id : '',
            ];
        }

        $writer = new \XLSXWriter();
        foreach ($rows as $row) {
            $writer->writeSheetRow('Sheet1', $row);
        }
        $writer->writeToStdOut();
        exit(0);
    }

}
