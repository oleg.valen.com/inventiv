<?php

namespace backend\controllers;

use common\models\DeliverySurveySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use Yii;

/**
 * DeliverySurveyController implements the CRUD actions for DeliverySurvey model.
 */
class B2bTableController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
//                    [
//                        'actions' => ['create', 'thanks', 'done'],
//                        'allow' => true,
//                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DeliverySurvey models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index', [
            'id' => Yii::$app->controller->id,
        ]);
    }

}
