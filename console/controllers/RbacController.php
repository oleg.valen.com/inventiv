<?php

namespace console\controllers;

use common\rbac\AuthorRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        //permissions
        $viewRetail = $auth->createPermission('viewRetail');
        $viewRetail->description = 'View retail';
        $auth->add($viewRetail);

        $viewDelivery = $auth->createPermission('viewDelivery');
        $viewDelivery->description = 'View delivery';
        $auth->add($viewDelivery);

        $viewB2b = $auth->createPermission('viewB2b');
        $viewB2b->description = 'View b2b';
        $auth->add($viewB2b);

        $viewAsc = $auth->createPermission('viewAsc');
        $viewAsc->description = 'View asc';
        $auth->add($viewAsc);

        $viewLogistics = $auth->createPermission('viewLogistics');
        $viewLogistics->description = 'View logistics';
        $auth->add($viewLogistics);

        //roles

        $retail = $auth->createRole('retail');
        $auth->add($retail);
        $auth->addChild($retail, $viewRetail);

        $delivery = $auth->createRole('delivery');
        $auth->add($delivery);
        $auth->addChild($delivery, $viewDelivery);

        $b2b = $auth->createRole('b2b');
        $auth->add($b2b);
        $auth->addChild($b2b, $viewB2b);

        $asc = $auth->createRole('asc');
        $auth->add($asc);
        $auth->addChild($asc, $viewAsc);

        $logist = $auth->createRole('logist');
        $auth->add($logist);
        $auth->addChild($logist, $viewLogistics);
        $auth->addChild($logist, $viewDelivery);

        $fullRights = $auth->createRole('fullRights');
        $auth->add($fullRights);
        $auth->addChild($fullRights, $retail);
        $auth->addChild($fullRights, $delivery);
        $auth->addChild($fullRights, $b2b);
        $auth->addChild($fullRights, $asc);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $fullRights);
        $auth->addChild($admin, $logist);

        //assigns
        $auth->assign($admin, 1);
        $auth->assign($admin, 6);

        $auth->assign($fullRights, 5);

        $auth->assign($retail, 4);
        $auth->assign($delivery, 4);

        $auth->assign($logist, 3);
    }
}