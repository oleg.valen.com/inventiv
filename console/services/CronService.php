<?php

namespace console\services;

use common\models\DeliverySurvey;
use common\models\DeliverySurveyStatus\DeliverySurveyContext;
use Yii;

class CronService
{

    public static $errors = [];
    public static $emailSentTotal = 0;

    public function manageNotifications()
    {
        $deliverySurveys = DeliverySurvey::find()
            ->where(['status' => DeliverySurvey::STATUS_INIT])
            ->andWhere(['in', 'nps', DeliverySurvey::BAD_NPS])
            ->andWhere(['not', ['nps' => null]])
            ->all();
        foreach ($deliverySurveys as $item) {
            $item->status = DeliverySurvey::STATUS_NEW;
            $item->save(false);
        }

        $deliverySurveys = DeliverySurvey::find()
            ->where(['in', 'nps', DeliverySurvey::BAD_NPS])
            ->andWhere(['not in', 'status', [
                DeliverySurvey::STATUS_INIT,
                DeliverySurvey::STATUS_CLOSED_FINAL,
                DeliverySurvey::STATUS_CONFIRMATION,
                DeliverySurvey::STATUS_NOT_CLOSED_FINAL,
            ]])
            ->andWhere(['>=', 'created_at', strtotime('2021-11-23')])
            ->all();

        foreach ($deliverySurveys as $item) {
            $class = '\common\models\DeliverySurveyStatus\\' . DeliverySurvey::STATUS_CLASSES[$item->status];
            $deliverySurveysContext = new DeliverySurveyContext($item, new $class(['statusUpdatedAt' => $item->status_updated_at]));
            $deliverySurveysContext->handle();
//            return; //test
        }
    }

}
