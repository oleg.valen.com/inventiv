<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/daterangepicker.css',
        'css/menu-style.css',
        'css/style.css',
    ];
    public $js = [
        'js/script.js',
        'js/moment.js',
        'js/daterangepicker.js',
        'js/charts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
