<?php

return [
    'viewRetail' => [
        'type' => 2,
        'description' => 'View retail',
    ],
    'viewDelivery' => [
        'type' => 2,
        'description' => 'View delivery',
    ],
    'viewB2b' => [
        'type' => 2,
        'description' => 'View b2b',
    ],
    'viewAsc' => [
        'type' => 2,
        'description' => 'View asc',
    ],
    'viewLogistics' => [
        'type' => 2,
        'description' => 'View logistics',
    ],
    'retail' => [
        'type' => 1,
        'children' => [
            'viewRetail',
        ],
    ],
    'delivery' => [
        'type' => 1,
        'children' => [
            'viewDelivery',
        ],
    ],
    'b2b' => [
        'type' => 1,
        'children' => [
            'viewB2b',
        ],
    ],
    'asc' => [
        'type' => 1,
        'children' => [
            'viewAsc',
        ],
    ],
    'logist' => [
        'type' => 1,
        'children' => [
            'viewLogistics',
            'viewDelivery',
        ],
    ],
    'fullRights' => [
        'type' => 1,
        'children' => [
            'retail',
            'delivery',
            'b2b',
            'asc',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'fullRights',
            'logist',
        ],
    ],
];
