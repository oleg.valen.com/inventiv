<?php

namespace common\models;

use yii\db\ActiveRecord;

class TransportCompany extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%transport_company}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Транспортная компания',
        ];
    }
}
