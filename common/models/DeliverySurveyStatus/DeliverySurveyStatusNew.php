<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyStatusNew extends DeliverySurveyStatus
{
    protected $nextStatus = DeliverySurvey::STATUS_EXPIRED;
    protected $levels = [1, 3];
    protected $subject = 'Новый алерт';
    protected $body = "В системе появился новый алерт\n";
}
