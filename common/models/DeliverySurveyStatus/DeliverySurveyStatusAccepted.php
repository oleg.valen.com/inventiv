<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyStatusAccepted extends DeliverySurveyStatus
{
    protected $nextStatus = DeliverySurvey::STATUS_CLOSED_FINAL;
    protected $levels = [1];
    protected $subject = 'Алерт принят';
    protected $body = "В системе появился принятый алерт\n";
    protected $sendAnyway = true;
}
