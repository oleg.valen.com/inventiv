<?php


namespace common\models\DeliverySurveyStatus;


use common\models\User;
use console\services\CronService;
use frontend\models\ContactForm;
use yii\helpers\Url;
use Yii;

abstract class DeliverySurveyStatus
{
    protected $context;
    protected $diffMinutes = 0;
    protected $nextStatus = null;
    protected $levels = [];
    protected $subject = '';
    protected $body = '';
    protected $message = "Для ознакомления перейдите по ссылке\n";
    protected $sendAnyway = true;

    public function __construct($params = [])
    {
        if (isset($params['statusUpdatedAt'])) {
            $this->diffMinutes = round(abs(time() - $params['statusUpdatedAt']) / 60, 2);
        }
    }

    public function setContext(DeliverySurveyContext $context)
    {
        $this->context = $context;
    }

    public function handle()
    {
        if ($this->nextStatus && ($this->diffMinutes > 120 || $this->sendAnyway)) {
            if ($this->send()) {
                $deliverySurvey = $this->context->getDeliverySurvey();
                $deliverySurvey->status = $this->nextStatus;
                $deliverySurvey->save(false);
            }
        }
    }

    public function send()
    {
        if (empty($this->levels))
            return true;

        $deliverySurvey = $this->context->getDeliverySurvey();
        $accesses = array_reduce($this->levels, function ($c, $i) {
            return array_merge($c, User::$levels[$i]);
        }, []);
        $users = User::find()
            ->where(['in', 'access', $accesses])
            ->andWhere(['survey_type' => User::SURVEY_TYPE_DELIVERY])
            ->andWhere(['not', ['email' => null]])
            ->all();

        foreach ($users as $item) {
            $form = new ContactForm([
//                'email' => 'oleg.valen.com@gmail.com', //test
                'email' => 'writeyulia@yandex.ru', //test
//                'email' => $item->email,
                'name' => $item->surname_name,
                'subject' => $this->subject,
//                'body' => $this->message . Yii::$app->request->hostInfo . Url::to(['delivery-survey/update', 'survey_id' => $deliverySurvey->survey_id]),
                'body' => $this->body .
                    $this->message .
                    'https://inventive-opros.ru/admin/delivery-survey/update?survey_id=' . $deliverySurvey->survey_id,
            ]);
            try {
//                $form->sendEmail('oleg.valen.com@gmail.com'); //test
                $form->sendEmail('writeyulia@yandex.ru'); //test
//                $form->sendEmail($item->email);
                CronService::$emailSentTotal++;
            } catch (\Exception $e) {
                Yii::error([
                    'name' => __METHOD__,
                    'time' => $start = microtime(true),
                    'error' => $e,
                ], 'api');
                return false;
            }
        }
        return true;
    }
}
