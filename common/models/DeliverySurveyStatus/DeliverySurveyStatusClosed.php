<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyStatusClosed extends DeliverySurveyStatus
{
    protected $nextStatus = DeliverySurvey::STATUS_CONFIRMATION;
    protected $levels = [2];
    protected $subject = 'Закрытый алерт на подтверждение';
    protected $body = "В системе появился закрытый алерт на подтверждение\n";
    protected $sendAnyway = true;
}
