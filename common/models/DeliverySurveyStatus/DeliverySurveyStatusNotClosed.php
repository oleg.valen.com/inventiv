<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyStatusNotClosed extends DeliverySurveyStatus
{
    protected $nextStatus = DeliverySurvey::STATUS_NOT_CLOSED_FINAL;
    protected $levels = [3];
    protected $subject = 'Незакрытый алерт';
    protected $bbody = "В системе есть все еще незакрытый алерт\n";
}
