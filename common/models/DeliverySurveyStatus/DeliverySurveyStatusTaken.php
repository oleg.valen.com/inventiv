<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyStatusTaken extends DeliverySurveyStatus
{
    protected $nextStatus = DeliverySurvey::STATUS_NOT_CLOSED;
    protected $levels = [2];
    protected $subject = 'Незакрытый алерт';
    protected $body = "В системе появился незакрытый алерт\n";
    protected $sendAnyway = true;
}
