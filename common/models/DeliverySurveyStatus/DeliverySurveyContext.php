<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyContext
{
    private $deliverySurvey;
    private $deliverySurveyStatus;

    public function __construct(DeliverySurvey $deliverySurvey, DeliverySurveyStatus $deliverySurveyStatus)
    {
        $this->transitionTo($deliverySurvey, $deliverySurveyStatus);
    }

    public function transitionTo(DeliverySurvey $deliverySurvey, DeliverySurveyStatus $deliverySurveyStatus)
    {
        $this->deliverySurvey = $deliverySurvey;
        $this->deliverySurveyStatus = $deliverySurveyStatus;
        $this->deliverySurveyStatus->setContext($this);
    }

    public function getDeliverySurvey()
    {
        return $this->deliverySurvey;
    }

    public function handle()
    {
        $this->deliverySurveyStatus->handle();
    }

}
