<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyStatusExpired extends DeliverySurveyStatus
{
    protected $nextStatus = DeliverySurvey::STATUS_NOT_CLOSED;
    protected $levels = [1, 2];
    protected $subject = 'Просроченный алерт';
    protected $body = "В системе появился просроченный алерт\n";
}
