<?php


namespace common\models\DeliverySurveyStatus;


use common\models\DeliverySurvey;

class DeliverySurveyStatusRevision extends DeliverySurveyStatus
{
    protected $nextStatus = DeliverySurvey::STATUS_TAKEN;
    protected $levels = [1];
    protected $subject = 'Алерт на доработку';
    protected $body = "В системе появился алерт на доработку\n";
}
