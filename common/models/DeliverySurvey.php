<?php


namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class DeliverySurvey extends ActiveRecord
{

    const STATUS_INIT = 0; //any new survey
    const STATUS_NEW = 1; //from this till the closed - has alert
    const STATUS_TAKEN = 2;
    const STATUS_EXPIRED = 3;
    const STATUS_NOT_CLOSED = 4;
    const STATUS_NOT_CLOSED_FINAL = 5; //in 2 hours after not closed
    const STATUS_CLOSED = 6;
    const STATUS_CONFIRMATION = 7;
    const STATUS_ACCEPTED = 8;
    const STATUS_REVISION = 9;
    const STATUS_CLOSED_FINAL = 10;

    const STATUS_CLASSES = [
        'DeliverySurveyStatusInit',
        'DeliverySurveyStatusNew',
        'DeliverySurveyStatusTaken',
        'DeliverySurveyStatusExpired',
        'DeliverySurveyStatusNotClosed',
        'DeliverySurveyStatusNotClosedFinal',
        'DeliverySurveyStatusClosed',
        'DeliverySurveyStatusConfirmation',
        'DeliverySurveyStatusAccepted',
        'DeliverySurveyStatusRevision',
        'DeliverySurveyStatusClosedFinal',
    ];

    const BAD_NPS = [0, 1, 2, 3, 4, 5, 6];
    const PASSIVE_NPS = [7, 8];
    const GOOD_NPS = [9, 10];

    public static $values = [
        'question1' => [
            1 => 'Очень плохо',
            2 => 'Плохо',
            3 => 'Удовлетворительно',
            4 => 'Хорошо',
            5 => 'Отлично',
        ],
        'question2' => [
            1 => 'Да, день и время соответствуют указанным в заказе',
            2 => 'Нет, приехал в назначенный день, но в другое время',
            3 => 'Нет, приехал в другой день',
        ],
        'question3' => [
            1 => 'Всё хорошо',
            2 => 'Не распаковывал(а)/Покупал(а) не себе',
            3 => 'Упаковка в пыли/испачкана',
            4 => 'Упаковка помята',
        ],
        'question4' => [
            1 => 'Позвонил не менее чем за полчаса',
            2 => 'Курьер позвонил только по приезде',
            3 => 'Курьер не предупредил о приезде',
        ],
    ];

    public static $statusValues = [
        DeliverySurvey::STATUS_INIT => 'Init',
        DeliverySurvey::STATUS_NEW => 'Новый',
        DeliverySurvey::STATUS_TAKEN => 'Взят в работу',
        DeliverySurvey::STATUS_EXPIRED => 'Просрочен',
        DeliverySurvey::STATUS_NOT_CLOSED => 'Просрочен', //Не закрыт
        DeliverySurvey::STATUS_NOT_CLOSED_FINAL => 'Просрочен', //Не закрыт
        DeliverySurvey::STATUS_CLOSED => 'На подтверждении', //Закрыт на подтверждение
        DeliverySurvey::STATUS_CONFIRMATION => 'На подтверждении',
        DeliverySurvey::STATUS_ACCEPTED => 'Закрыт', //Принят
        DeliverySurvey::STATUS_REVISION => 'Взят в работу', //На доработку
        DeliverySurvey::STATUS_CLOSED_FINAL => 'Закрыт',
    ];

    protected $oldStatus;

    public static function tableName()
    {
        return '{{delivery_survey}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
//                // если вместо метки времени UNIX используется datetime:
//                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function rules()
    {
        return [
            [['client_id', 'doc_id'], 'required'],
            [['client_id', 'doc_id', 'nps', 'question1', 'question2', 'question3', 'question4'], 'integer'],
            [['survey_id', 'nps', 'question1', 'question2', 'question3', 'question4', 'status', 'comment', 'sms_sent',
                'sms_sent_id', 'sms_sent_status_error', 'created_at', 'updated_at', 'status_updated_at', 'revision_comment'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'survey_id' => 'Номер анкеты',
            'client_id' => 'ID клиента',
            'doc_id' => 'Номер документа',
            'NPS' => 'NPS',
            'question1' => 'Как вы оцениваете доставку товара?',
            'question2' => 'Курьер приехал в согласованное время доставки?',
            'question3' => 'Как вы оцениваете сохранность упаковки самого товара?',
            'question4' => 'Курьер предупредил ли заранее о своем приезде?',
            'status' => 'Статус',
            'sms_sent' => 'SMS отправлено',
            'sms_sent_id' => 'ID sms',
            'sms_sent_satus_error' => 'ID ошибки sms',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'status_updated_at' => 'Дата обновления статуса',
        ];
    }

    public function afterFind()
    {
        $this->oldStatus = $this->status;
        return parent::afterFind();
    }

    public function beforeSave($options = [])
    {
        if ($this->status != $this->oldStatus)
            $this->status_updated_at = time();

        return parent::beforeSave($this->isNewRecord);
    }

    public function getDeliveryDoc()
    {
        return $this->hasOne(DeliveryDoc::className(), ['doc_id' => 'doc_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['survey_id' => 'survey_id'])
            ->andOnCondition(['type_id' => Log::TYPE_DELIVERY]);
    }

}
