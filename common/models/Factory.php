<?php

namespace common\models;

use yii\db\ActiveRecord;

class Factory extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%factory}}';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Завод',
        ];
    }
}
