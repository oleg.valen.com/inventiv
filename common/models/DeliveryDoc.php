<?php

namespace common\models;

use yii\db\ActiveRecord;

class DeliveryDoc extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%delivery_doc}}';
    }

    public function attributeLabels()
    {
        return [
            'doc_id' => 'ID',
            'numdoc_1c' => '№ документа из 1С',
            'sum' => 'Сумма с НДС',
            'delivery_date' => 'Дата доставки',
            'time_range' => 'Временной диапазон',
        ];
    }

    public function getFactory()
    {
        return $this->hasOne(Factory::className(), ['id' => 'factory_id']);
    }

    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    public function getTransportCompany()
    {
        return $this->hasOne(TransportCompany::className(), ['id' => 'transport_company_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getStatusDelivery()
    {
        return $this->hasOne(StatusDelivery::className(), ['id' => 'status_delivery_id']);
    }

    public function getDeliverySurvey()
    {
        return $this->hasOne(DeliverySurvey::className(), ['doc_id' => 'doc_id']);
    }

}
