<?php

namespace common\models;

use yii\db\ActiveRecord;
use Yii;

class ClientToken extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%client_token}}';
    }

    public function generateToken()
    {
        $this->token = Yii::$app->security->generateRandomString(10);
    }

//    public function validate($attributeNames = NULL, $clearErrors = true)
//    {
//        $get = Yii::$app->request->get();
//
//        $result = parent::validate($attributeNames, $clearErrors);
//        if ($result === false) {
//            return $result;
//        }
//
//        $model = ClientToken::find()
//            ->where(['token' =>  $get['token']])
//            ->one();
//        return $model ?? false;
//    }

}
